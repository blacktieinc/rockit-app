import moment from 'moment';
import i18n from 'i18next';
import { initReactI18next } from 'react-i18next';
import { AppRegistry } from 'react-native';
import { App } from '@/app.component';
import { en } from '@/locale/en';
import { name as appName } from './app.json';
import 'moment/locale/uk';

moment.locale('uk');

// eslint-disable-next-line no-console
console.disableYellowBox = true;

i18n
  .use(initReactI18next) // passes i18n down to react-i18next
  .init({
    resources: {
      en: {
        translation: en
      }
    },
    lng: 'en',
    fallbackLng: 'en',
    interpolation: {
      escapeValue: false
    }
  });


AppRegistry.registerComponent(appName, () => App);
