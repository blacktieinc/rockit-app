module.exports = {
  parser: "@typescript-eslint/parser",
  plugins: ["@typescript-eslint", "prettier", "react", "import", "react-hooks"],
  extends: [
    "airbnb-typescript",
    "plugin:@typescript-eslint/recommended",
    "plugin:react/recommended",
    "plugin:prettier/recommended",
    "prettier/@typescript-eslint",
    "prettier/react"
  ],
  parserOptions:  {
    ecmaVersion:  2018,
    sourceType:  'module',
    ecmaFeatures:  {
      jsx:  true,
    },
  },
  rules: {
    "react-hooks/rules-of-hooks": "error",
    "react-hooks/exhaustive-deps": "off",
    "import/prefer-default-export": "off",
    "max-classes-per-file": 'off',
    "no-empty-function": "off",
    "no-useless-constructor": "off",
    "lines-between-class-members": "off",
    "@typescript-eslint/no-namespace": "off",
    "@typescript-eslint/no-parameter-properties": "off",
    "react/self-closing-comp": ["error", {
      "component": true,
      "html": true
    }],
    "react-hooks/exhaustive-deps": "warn",
    "import/no-extraneous-dependencies": ["error", {"devDependencies": true}]
  },
  settings:  {
    react:  {
      version:  'detect',
    },
    "import/parsers": {
      "@typescript-eslint/parser": [".ts", ".tsx"]
    },
    'import/resolver': {
      typescript: {}
    }
  },
  env: {
    "node": true,
    "es6": true,
    "browser": true
  }
};