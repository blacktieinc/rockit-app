module.exports = {
  presets: ['module:metro-react-native-babel-preset'],
  plugins: [
    [
      require.resolve('babel-plugin-module-resolver'),
      {
        root: ['./src'],
        alias: {
          '@': './src',
          '@theme': './src/theme',
          '@screens': './src/screens/',
          '@lightboxes': './src/lightboxes/',
          '@store': './src/store/',
          '@uikit': './src/uikit/',
          '@shared': './src/_shared/',
          '@common': './src/_common/'
        }
      }
    ]
  ]
};
