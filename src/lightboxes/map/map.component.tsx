import React from 'react';
import get from 'lodash/get';
import MapView, { Marker } from 'react-native-maps';
import { Lightbox, Icon } from '@uikit/components';
import { variable } from '@uikit/theme';
import { styles } from './map.styles';

interface MapLightboxProps {
  location: DTO.Location;
}

export function MapLightbox({ location }: MapLightboxProps): JSX.Element {
  return (
    <Lightbox>
      <MapView
        style={styles.map}
        initialRegion={{
          latitude: get(location, 'geometry.lat'),
          longitude: get(location, 'geometry.lng'),
          latitudeDelta: 0.03,
          longitudeDelta: 0.03
        }}
      >
        <Marker
          coordinate={{
            latitude: get(location, 'geometry.lat'),
            longitude: get(location, 'geometry.lng')
          }}
        >
          <Icon name='location' size={40} color={variable.brandPrimary} />
        </Marker>
      </MapView>
    </Lightbox>
  );
}
