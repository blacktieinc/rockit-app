import { ViewStyle } from 'react-native';
import StyleSheet from 'react-native-extended-stylesheet';
import { variable } from '@uikit/theme';

interface Styles {
  map: ViewStyle;
}

export const styles: Styles = StyleSheet.create({
  map: {
    width: '100%',
    height: (variable.deviceHeight / 16) * 13 - 100
  }
} as Styles) as Styles;
