import { useCallback } from 'react';
import { Toast as NBToast } from 'native-base';

interface ToastParams {
  message: string;
  button?: string;
  duration?: number;
}

type ToastFn = (params: ToastParams) => void;

type ToastType = 'danger' | 'success';

interface Toast {
  success: ToastFn;
  danger: ToastFn;
}

function toast(
  type: ToastType,
  { message, button, duration = 10000 }: ToastParams
): void {
  NBToast.show({
    type,
    duration,
    text: message,
    buttonText: button
  });
}

export function useToast(): Toast {
  const danger = useCallback((params: ToastParams): void => {
    toast('danger', params);
  }, []);

  const success = useCallback((params: ToastParams): void => {
    toast('success', params);
  }, []);

  return { success, danger };
}
