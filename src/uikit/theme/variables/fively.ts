import color from 'color';
import { Platform, Dimensions, PixelRatio } from 'react-native';

export const PLATFORM = {
  ANDROID: 'android',
  IOS: 'ios',
  MATERIAL: 'material',
  WEB: 'web'
};

const deviceHeight = Dimensions.get('window').height;
const deviceWidth = Dimensions.get('window').width;
const platform = Platform.OS;
const platformStyle = undefined;
const isIphoneX =
  platform === PLATFORM.IOS &&
  (deviceHeight === 812 ||
    deviceWidth === 812 ||
    deviceHeight === 896 ||
    deviceWidth === 896);

export class Fively {
  public platformStyle = platformStyle;
  public platform = platform;

  // Accordion
  public accordionBorderColor = '#d3d3d3';
  public accordionContentPadding = 10;
  public accordionIconFontSize = 18;
  public contentStyle = '#f5f4f5';
  public expandedIconStyle = '#000';
  public headerStyle = '#edebed';
  public iconStyle = '#000';

  // ActionSheet
  public elevation = 4;
  public containerTouchableBackgroundColor = 'rgba(0,0,0,0.4)';
  public innerTouchableBackgroundColor = '#fff';
  public listItemHeight = 50;
  public listItemBorderColor = 'transparent';
  public marginHorizontal = -15;
  public marginLeft = 14;
  public marginTop = 15;
  public minHeight = 56;
  public padding = 15;
  public touchableTextColor = '#757575';

  // Android
  public androidRipple = true;
  public androidRippleColor = 'rgba(256, 256, 256, 0.3)';
  public androidRippleColorDark = 'rgba(0, 0, 0, 0.15)';
  public buttonUppercaseAndroidText = true;

  //   Badge
  public badgeBg = '#ED1727';
  public badgeColor = '#fff';
  public badgePadding = platform === PLATFORM.IOS ? 6 : 0;

  // Button
  public buttonFontFamily =
    platform === PLATFORM.IOS ? 'Montserrat' : 'Roboto_medium';
  public buttonDisabledBg = '#b5b5b5';
  public buttonPadding = 6;
  public buttonDefaultActiveOpacity = 0.5;
  public buttonDefaultFlex = 1;
  public buttonDefaultBorderRadius = 2;
  public buttonDefaultBorderWidth = 1;
  public get buttonPrimaryBg(): string {
    return this.brandPrimary;
  }
  public get buttonPrimaryColor(): string {
    return this.inverseTextColor;
  }
  public get buttonInfoBg(): string {
    return this.brandInfo;
  }
  public get buttonInfoColor(): string {
    return this.inverseTextColor;
  }
  public get buttonSuccessBg(): string {
    return this.brandSuccess;
  }
  public get buttonSuccessColor(): string {
    return this.inverseTextColor;
  }
  public get buttonDangerBg(): string {
    return this.brandDanger;
  }
  public get buttonDangerColor(): string {
    return this.inverseTextColor;
  }
  public get buttonWarningBg(): string {
    return this.brandWarning;
  }
  public get buttonWarningColor(): string {
    return this.inverseTextColor;
  }
  public get buttonTextSize(): number {
    return this.fontSizeBase * 0.875;
  }
  public get buttonTextSizeLarge(): number {
    return this.fontSizeBase * 1.5;
  }
  public get buttonTextSizeSmall(): number {
    return this.fontSizeBase * 0.8;
  }
  public get borderRadiusLarge(): number {
    return this.fontSizeBase * 3.8;
  }
  public get iconSizeLarge(): number {
    return this.iconFontSize * 1.5;
  }
  public get iconSizeSmall(): number {
    return this.iconFontSize * 0.6;
  }

  // Card
  public cardDefaultBg = '#fff';
  public cardBorderColor = 'transparent';
  public cardBorderRadius = 8;
  public cardItemPadding = platform === PLATFORM.IOS ? 10 : 12;

  // CheckBox
  public CheckboxRadius = platform === PLATFORM.IOS ? 13 : 0;
  public CheckboxBorderWidth = platform === PLATFORM.IOS ? 1 : 2;
  public CheckboxPaddingLeft = platform === PLATFORM.IOS ? 4 : 2;
  public CheckboxPaddingBottom = platform === PLATFORM.IOS ? 0 : 5;
  public CheckboxIconSize = platform === PLATFORM.IOS ? 21 : 16;
  public CheckboxIconMarginTop = platform === PLATFORM.IOS ? undefined : 1;
  public CheckboxFontSize = platform === PLATFORM.IOS ? 23 / 0.9 : 17;
  public checkboxBgColor = '#039BE5';
  public checkboxSize = 20;
  public checkboxTickColor = '#fff';
  public checkboxDefaultColor = 'transparent';
  public checkboxTextShadowRadius = 0;

  // Color
  public brandPrimary = platform === PLATFORM.IOS ? '#DB3022' : '#3F51B5';
  public brandInfo = '#62B1F6';
  public brandSuccess = '#2AA952';
  public brandDanger = '#DB3022';
  public brandWarning = '#f0ad4e';
  public brandDark = '#222';
  public brandLight = '#fff';

  // Container
  public containerBgColor = '#F9F9F9';

  // Date Picker
  public datePickerFlex = 1;
  public datePickerPadding = 10;
  public datePickerTextColor = '#000';
  public datePickerBg = 'transparent';

  // FAB
  public fabBackgroundColor = 'blue';
  public fabBorderRadius = 28;
  public fabBottom = 0;
  public fabButtonBorderRadius = 20;
  public fabButtonHeight = 40;
  public fabButtonLeft = 7;
  public fabButtonMarginBottom = 10;
  public fabContainerBottom = 20;
  public fabDefaultPosition = 20;
  public fabElevation = 4;
  public fabIconColor = '#fff';
  public fabIconSize = 24;
  public fabShadowColor = '#000';
  public fabShadowOffsetHeight = 2;
  public fabShadowOffsetWidth = 0;
  public fabShadowOpacity = 0.4;
  public fabShadowRadius = 2;
  public fabWidth = 56;

  // Font
  public DefaultFontSize = 16;
  public fontFamily = platform === PLATFORM.IOS ? 'Montserrat' : 'Roboto';
  public fontSizeBase = 16;
  public get fontSizeH1(): number {
    return this.fontSizeBase * 2.125;
  }
  public get fontSizeH2(): number {
    return this.fontSizeBase * 1.5;
  }
  public get fontSizeH3(): number {
    return this.fontSizeBase * 1.125;
  }

  // Footer
  public footerHeight = 55;
  public footerDefaultBg = platform === PLATFORM.IOS ? '#fff' : '#fff';
  public footerPaddingBottom = 0;

  // FooterTab
  public tabBarTextColor = platform === PLATFORM.IOS ? '#6b6b6b' : '#b3c7f9';
  public tabBarTextSize = platform === PLATFORM.IOS ? 14 : 11;
  public activeTab = platform === PLATFORM.IOS ? '#007aff' : '#fff';
  public sTabBarActiveTextColor = '#007aff';
  public tabBarActiveTextColor = platform === PLATFORM.IOS ? '#007aff' : '#fff';
  public tabActiveBgColor = platform === PLATFORM.IOS ? '#cde1f9' : '#3F51B5';

  // Header
  public toolbarBtnColor = platform === PLATFORM.IOS ? '#007aff' : '#fff';
  public toolbarDefaultBg = platform === PLATFORM.IOS ? '#F9F9F9' : '#3F51B5';
  public toolbarHeight = platform === PLATFORM.IOS ? 64 : 56;
  public toolbarSearchIconSize = platform === PLATFORM.IOS ? 20 : 23;
  public toolbarInputColor = platform === PLATFORM.IOS ? '#CECDD2' : '#fff';
  public searchBarHeight = platform === PLATFORM.IOS ? 30 : 40;
  public searchBarInputHeight = platform === PLATFORM.IOS ? 30 : 50;
  public toolbarBtnTextColor = platform === PLATFORM.IOS ? '#007aff' : '#fff';
  public toolbarDefaultBorder =
    platform === PLATFORM.IOS ? 'transparent' : '#3F51B5';
  public iosStatusbar =
    platform === PLATFORM.IOS ? 'dark-content' : 'light-content';
  public get statusBarColor(): string {
    return color(this.toolbarDefaultBg)
      .darken(0.2)
      .hex();
  }
  public get darkenHeader(): string {
    return color(this.tabBgColor)
      .darken(0.03)
      .hex();
  }

  // Icon
  public iconFamily = 'Ionicons';
  public iconFontSize = platform === PLATFORM.IOS ? 30 : 28;
  public iconHeaderSize = platform === PLATFORM.IOS ? 33 : 24;

  // InputGroup
  public inputFontSize = 14;
  public inputBorderColor = '#D9D5DC';
  public inputSuccessBorderColor = '#2b8339';
  public inputErrorBorderColor = '#ed2f2f';
  public inputHeightBase = 20;
  public get inputColor(): string {
    return this.textColor;
  }
  public inputColorPlaceholder = '#575757';

  // Line Height
  public buttonLineHeight = 19;
  public lineHeightH1 = this.fontSizeH1 + 1;
  public lineHeightH2 = 29;
  public lineHeightH3 = this.fontSizeH3;
  public lineHeight = platform === PLATFORM.IOS ? 20 : 24;
  public listItemSelected = platform === PLATFORM.IOS ? '#007aff' : '#3F51B5';

  // List
  public listBg = 'transparent';
  public listBorderColor = '#c9c9c9';
  public listDividerBg = '#f4f4f4';
  public listBtnUnderlayColor = '#DDD';
  public listItemPadding = platform === PLATFORM.IOS ? 10 : 12;
  public listNoteColor = '#808080';
  public listNoteSize = 13;

  // Progress Bar
  public defaultProgressColor = '#E4202D';
  public inverseProgressColor = '#1A191B';

  // Radio Button
  public radioBtnSize = platform === PLATFORM.IOS ? 25 : 23;
  public radioSelectedColorAndroid = '#3F51B5';
  public radioBtnLineHeight = platform === PLATFORM.IOS ? 29 : 24;
  public get radioColor(): string {
    return this.brandPrimary;
  }

  // Segment
  public segmentBackgroundColor =
    platform === PLATFORM.IOS ? '#F8F8F8' : '#3F51B5';
  public segmentActiveBackgroundColor =
    platform === PLATFORM.IOS ? '#007aff' : '#fff';
  public segmentTextColor = platform === PLATFORM.IOS ? '#007aff' : '#fff';
  public segmentActiveTextColor =
    platform === PLATFORM.IOS ? '#fff' : '#3F51B5';
  public segmentBorderColor = platform === PLATFORM.IOS ? '#007aff' : '#fff';
  public segmentBorderColorMain =
    platform === PLATFORM.IOS ? '#a7a6ab' : '#3F51B5';

  // Spinner
  public defaultSpinnerColor = '#45D56E';
  public inverseSpinnerColor = '#1A191B';

  // Tab
  public tabBarDisabledTextColor = '#BDBDBD';
  public tabDefaultBg = platform === PLATFORM.IOS ? '#F8F8F8' : '#3F51B5';
  public topTabBarTextColor = platform === PLATFORM.IOS ? '#6b6b6b' : '#b3c7f9';
  public topTabBarActiveTextColor =
    platform === PLATFORM.IOS ? '#007aff' : '#fff';
  public topTabBarBorderColor = platform === PLATFORM.IOS ? '#a7a6ab' : '#fff';
  public topTabBarActiveBorderColor =
    platform === PLATFORM.IOS ? '#007aff' : '#fff';

  // Tabs
  public tabBgColor = '#F8F8F8';
  public tabFontSize = 15;

  // Text
  public textColor = '#222';
  public inverseTextColor = '#fff';
  public noteFontSize = 14;
  public get defaultTextColor(): string {
    return this.textColor;
  }

  // Title
  public titleFontfamily =
    platform === PLATFORM.IOS ? 'Montserrat' : 'Roboto_medium';
  public titleFontSize = platform === PLATFORM.IOS ? 18 : 19;
  public subTitleFontSize = platform === PLATFORM.IOS ? 11 : 14;
  public subtitleColor = platform === PLATFORM.IOS ? '#8e8e93' : '#FFF';
  public titleFontColor = platform === PLATFORM.IOS ? '#222' : '#FFF';

  // Other
  public borderRadiusBase = platform === PLATFORM.IOS ? 5 : 2;
  public borderWidth = 1 / PixelRatio.getPixelSizeForLayoutSize(1);
  public contentPadding = 10;
  public dropdownLinkColor = '#414142';
  public inputLineHeight = 20;
  public deviceWidth = deviceWidth;
  public deviceHeight = deviceHeight;
  public isIphoneX = isIphoneX;
  public inputGroupRoundedBorderRadius = 30;

  // iPhoneX SafeArea
  public Inset = {
    portrait: {
      topInset: 24,
      leftInset: 0,
      rightInset: 0,
      bottomInset: 34
    },
    landscape: {
      topInset: 0,
      leftInset: 44,
      rightInset: 44,
      bottomInset: 21
    }
  };
}

export const variable = new Fively();
