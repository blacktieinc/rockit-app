import React, { PropsWithChildren } from 'react';
import { StyleProvider } from 'native-base';
import getTheme from '../components';
import fively from '../variables/fively';

export function Theme({ children }: PropsWithChildren<{}>): JSX.Element {
  return <StyleProvider style={getTheme(fively)}>{children}</StyleProvider>;
}
