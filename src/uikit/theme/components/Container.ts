import { Platform, Dimensions } from 'react-native';
import { variable, PLATFORM, Fively } from '../variables/fively';

const deviceHeight = Dimensions.get('window').height;

export default (variables: Fively = variable) => {
  const theme = {
    flex: 1,
    height: Platform.OS === PLATFORM.IOS ? deviceHeight : deviceHeight - 20,
    backgroundColor: variables.containerBgColor
  };

  return theme;
};
