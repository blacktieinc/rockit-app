import { variable, PLATFORM, Fively } from '../variables/fively';

export default (variables: Fively = variable) => {
  const h1Theme = {
    color: variables.textColor,
    fontSize: variables.fontSizeH1,
    lineHeight: variables.lineHeightH1,
    fontFamily: variable.fontFamily,
    fontWeight: 'bold',
    '.header': {
      marginBottom: 11
    }
  };

  return h1Theme;
};
