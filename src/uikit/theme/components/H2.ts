import { variable, PLATFORM, Fively } from '../variables/fively';

export default (variables: Fively = variable) => {
  const h2Theme = {
    color: variables.textColor,
    fontSize: variables.fontSizeH2,
    lineHeight: variables.lineHeightH2,
    fontWeight: '600'
  };

  return h2Theme;
};
