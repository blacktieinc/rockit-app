import { variable, PLATFORM, Fively } from '../variables/fively';

export default (variables: Fively = variable) => {
  const viewTheme = {
    '.padder': {
      padding: variables.contentPadding
    }
  };

  return viewTheme;
};
