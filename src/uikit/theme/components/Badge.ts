import { variable, PLATFORM, Fively } from '../variables/fively';

export default (variables: Fively = variable) => {
  const badgeTheme = {
    '.primary': {
      backgroundColor: variables.buttonPrimaryBg
    },
    '.warning': {
      backgroundColor: variables.buttonWarningBg
    },
    '.info': {
      backgroundColor: variables.buttonInfoBg
    },
    '.success': {
      backgroundColor: variables.buttonSuccessBg
    },
    '.danger': {
      backgroundColor: variables.buttonDangerBg
    },
    'NativeBase.Text': {
      color: variables.badgeColor,
      fontSize: 11,
      lineHeight: variables.lineHeight - 1,
      textAlign: 'center',
      paddingHorizontal: 3,
      fontWeight: '600'
    },
    backgroundColor: variables.badgeBg,
    padding: variables.badgePadding,
    alignSelf: 'flex-start',
    justifyContent: variables.platform === PLATFORM.IOS ? 'center' : undefined,
    borderRadius: 15
    // height: 27
  };
  return badgeTheme;
};
