export default () => {
  const labelTheme = {
    '.focused': {
      width: 0
    },
    fontSize: 14,
    lineHeight: 20,
    color: '#9B9B9B',
    fontWeight: '500'
  };

  return labelTheme;
};
