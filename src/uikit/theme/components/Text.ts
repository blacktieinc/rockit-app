import { variable, PLATFORM, Fively } from '../variables/fively';

export default (variables: Fively = variable) => {
  const textTheme = {
    fontSize: variables.DefaultFontSize,
    fontFamily: variables.fontFamily,
    color: variables.textColor,
    '.note': {
      color: '#9B9B9B',
      fontSize: variables.noteFontSize
    }
  };

  return textTheme;
};
