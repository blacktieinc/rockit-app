import { variable, PLATFORM, Fively } from '../variables/fively';

export default (variables: Fively = variable) => {
  const iconTheme = {
    fontSize: variables.iconFontSize,
    color: variable.textColor
  };

  return iconTheme;
};
