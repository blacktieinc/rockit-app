import React, { useMemo } from 'react';
import { ViewStyle } from 'react-native';
import { View, H2, Grid, Row, Col, Text } from 'native-base';
import { styles } from './heading.styles';

interface Note {
  label: string;
  value: JSX.Element;
}

interface HeadingProps {
  title?: string;
  style?: ViewStyle;
  notes?: Note[];
}

export function Heading({ title, style, notes }: HeadingProps): JSX.Element {
  const grid = useMemo(() => {
    return notes ? (
      <Grid>
        {notes.map(
          ({ label, value }): JSX.Element => {
            return (
              <Row key={label} style={styles.row}>
                <Col style={styles.colLabel}>
                  <Text note>{label}</Text>
                </Col>
                <Col style={styles.colAction}>{value}</Col>
              </Row>
            );
          }
        )}
      </Grid>
    ) : null;
  }, [notes]);

  const heading = title ? <H2 style={styles.title}>{title}</H2> : null;

  return (
    <View style={[style]}>
      {heading}
      {grid}
    </View>
  );
}
