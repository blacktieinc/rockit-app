import { ViewStyle, TextStyle } from 'react-native';
import StyleSheet from 'react-native-extended-stylesheet';

interface Styles {
  title: TextStyle;
  row: ViewStyle;
  colLabel: ViewStyle;
  colAction: ViewStyle;
}

export const styles: Styles = StyleSheet.create({
  title: {
    marginBottom: 4
  },
  row: {
    marginBottom: 4
  },
  colLabel: {
    flex: 0,
    flexShrink: 0,
    paddingTop: 2
  },
  colAction: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'flex-end',
    paddingLeft: 16
  }
} as Styles) as Styles;
