import React from 'react';
import ImageWithPropgress from 'react-native-image-progress';
import { Circle } from 'react-native-progress';
import { ImageStyle } from 'react-native';

interface ImageProps {
  uri: string;
  style?: ImageStyle;
}

export function Image({ uri, style }: ImageProps): JSX.Element {
  return (
    <ImageWithPropgress
      style={style}
      imageStyle={style}
      source={{ uri }}
      indicator={Circle}
      indicatorProps={{ size: 80 }}
    />
  );
}
