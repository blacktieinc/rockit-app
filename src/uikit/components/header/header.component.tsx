import React, { PropsWithChildren } from 'react';
import hexToRgba from 'hex-to-rgba';
import { Animated } from 'react-native';
import { Header as NbHeader } from 'native-base';
import { Body, inputRange } from './body/body.compoent';
import { variable } from '../../theme/variables/fively';

interface HeaderProps {
  scrollY: Animated.Value;
  input?: number[];
}

const AnimatedHeader = Animated.createAnimatedComponent(NbHeader);

export function Header({
  scrollY,
  children,
  input = inputRange
}: PropsWithChildren<HeaderProps>): JSX.Element {
  const shadowOpacity = scrollY.interpolate({
    inputRange: input,
    outputRange: [0, 0.08],
    extrapolate: 'clamp'
  });

  const backgroundColor = scrollY.interpolate({
    inputRange: input,
    outputRange: [
      hexToRgba(variable.toolbarDefaultBg, 0),
      hexToRgba(variable.toolbarDefaultBg, 1)
    ],
    extrapolate: 'clamp'
  });

  return (
    <AnimatedHeader style={{ shadowOpacity, backgroundColor }}>
      {children}
    </AnimatedHeader>
  );
}

Header.Body = Body;
