import React, { PropsWithChildren } from 'react';
import { Animated } from 'react-native';
import { Body as NbBody } from 'native-base';
import { variable } from '../../../theme/variables/fively';

const AnimatedBody = Animated.createAnimatedComponent(NbBody);

interface BodyProps {
  scrollY: Animated.Value;
  input?: number[];
}

export const inputRange = [18, variable.toolbarHeight + 18];

export function Body({
  scrollY,
  children,
  input = inputRange
}: PropsWithChildren<BodyProps>): JSX.Element {
  const opacity = scrollY.interpolate({
    inputRange: input,
    outputRange: [0, 1],
    extrapolate: 'clamp'
  });
  return <AnimatedBody style={{ opacity }}>{children}</AnimatedBody>;
}
