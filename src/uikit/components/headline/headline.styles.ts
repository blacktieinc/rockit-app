import { ViewStyle } from 'react-native';
import StyleSheet from 'react-native-extended-stylesheet';

interface Styles {
  heading: ViewStyle;
}

export const styles: Styles = StyleSheet.create({
  heading: {
    marginBottom: 11,
    marginTop: 18
  }
} as Styles) as Styles;
