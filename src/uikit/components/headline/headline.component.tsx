import React, { PropsWithChildren } from 'react';
import { TextStyle } from 'react-native';
import { H1 } from 'native-base';
import { styles } from './headline.styles';

interface HeadlineProps {
  children: string;
  style?: TextStyle;
}

export function Headline({
  children,
  style
}: PropsWithChildren<HeadlineProps>): JSX.Element {
  return <H1 style={[styles.heading, style]}>{children}</H1>;
}
