import React, { useMemo, useCallback } from 'react';
import { FlatList, Image, ViewStyle } from 'react-native';
import { View, Button, Text, H3, Badge } from 'native-base';
import { useTranslation } from 'react-i18next';
import { PreviewListItem } from './item/item.component';
import {
  PreviewListHeading,
  PreviewListHeadingLayout
} from './heading/heading.component';
import { styles } from './preview-list.styles';

interface PreviewListProps<T> {
  title: string;
  data: T[];
  keyExtractor: (item: T) => string;
  imageExtractor: (item: T) => string;
  titleExtractor: (item: T) => string;
  labelExtractor?: (item: T) => string;
  onItemPress?: (item: T) => void;
  onViewAllPress?: () => void;
  heading?: PreviewListHeadingLayout;
  style?: ViewStyle;
}

export function PreviewList<T = {}>({
  title,
  data = [],
  keyExtractor,
  imageExtractor,
  titleExtractor,
  labelExtractor,
  onItemPress,
  onViewAllPress,
  heading = 'h1',
  style
}: PreviewListProps<T>): JSX.Element {
  const { t } = useTranslation();

  const renderLabel = useCallback(
    (item: T) => {
      const label = labelExtractor && labelExtractor(item);
      return label ? (
        <Badge style={styles.itemLabel}>
          <Text>{labelExtractor(item)}</Text>
        </Badge>
      ) : null;
    },
    [labelExtractor]
  );

  const renderItem = (item: T, index: number): JSX.Element => {
    return (
      <PreviewListItem
        onPress={onItemPress ? (): void => onItemPress(item) : null}
        first={index === 0}
        last={index === data.length - 1}
      >
        <Image
          style={styles.itemImage}
          source={{ uri: imageExtractor(item) }}
        />
        {renderLabel(item)}
        <H3>{titleExtractor(item)}</H3>
      </PreviewListItem>
    );
  };

  const viewAllButton = useMemo(() => {
    return onViewAllPress ? (
      <Button
        dark
        transparent
        small
        style={styles.viewAllBtn}
        onPress={onViewAllPress}
      >
        <Text style={styles.viewAllText}>{t('buttons.viewAll')}</Text>
      </Button>
    ) : null;
  }, [onViewAllPress, t]);

  return (
    <View style={style}>
      <View style={styles.header}>
        <PreviewListHeading layout={heading}>{title}</PreviewListHeading>
        {viewAllButton}
      </View>
      <FlatList
        horizontal
        data={data}
        renderItem={({ item, index }): JSX.Element => renderItem(item, index)}
        keyExtractor={keyExtractor}
        showsHorizontalScrollIndicator={false}
      />
    </View>
  );
}
