import React, { PropsWithChildren } from 'react';
import { TouchableOpacity } from 'react-native';
import { View } from 'native-base';
import { styles } from './item.styles';

interface PreviewListItemProps {
  first: boolean;
  last: boolean;
  onPress?: () => void;
}

export function PreviewListItem({
  onPress,
  first,
  last,
  children
}: PropsWithChildren<PreviewListItemProps>): JSX.Element {
  const style = [
    styles.item,
    first && styles.itemFirst,
    last && styles.itemLast
  ];
  if (onPress) {
    return (
      <TouchableOpacity style={style} onPress={onPress}>
        {children}
      </TouchableOpacity>
    );
  }
  return <View style={style}>{children}</View>;
}
