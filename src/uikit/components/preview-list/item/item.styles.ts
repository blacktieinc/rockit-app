import { ViewStyle, ImageStyle } from 'react-native';
import StyleSheet from 'react-native-extended-stylesheet';

interface Styles {
  wrapper: ViewStyle;
  header: ViewStyle;
  item: ViewStyle;
  itemImage: ImageStyle;
  itemFirst: ViewStyle;
  itemLast: ViewStyle;
}

export const styles: Styles = StyleSheet.create({
  item: {
    width: 148,
    marginHorizontal: 9,
    position: 'relative'
  },
  itemFirst: {
    marginLeft: 16
  },
  itemLast: {
    marginRight: 16
  }
} as Styles) as Styles;
