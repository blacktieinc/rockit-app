import React, { PropsWithChildren } from 'react';
import { H1, H2, H3 } from 'native-base';
import { styles } from './heading.styles';

export type PreviewListHeadingLayout = 'h1' | 'h2' | 'h3';

interface PreviewListHeadingProps {
  layout: PreviewListHeadingLayout;
}

export function PreviewListHeading({
  children,
  layout
}: PropsWithChildren<PreviewListHeadingProps>): JSX.Element {
  if (layout === 'h1') {
    return <H1 style={styles.h1}>{children}</H1>;
  }
  if (layout === 'h2') {
    return <H2 style={styles.h2}>{children}</H2>;
  }
  if (layout === 'h3') {
    return <H3 style={styles.h3}>{children}</H3>;
  }
  return <></>;
}
