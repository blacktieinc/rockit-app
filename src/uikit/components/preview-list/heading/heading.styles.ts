import { TextStyle } from 'react-native';
import StyleSheet from 'react-native-extended-stylesheet';

interface Styles {
  h1: TextStyle;
  h2: TextStyle;
  h3: TextStyle;
}

export const styles: Styles = StyleSheet.create({
  h1: {},
  h2: {},
  h3: {
    fontSize: 18,
    lineHeight: 22
  }
} as Styles) as Styles;
