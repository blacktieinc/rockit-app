import { ViewStyle, ImageStyle, TextStyle } from 'react-native';
import StyleSheet from 'react-native-extended-stylesheet';

interface Styles {
  wrapper: ViewStyle;
  header: ViewStyle;
  viewAllBtn: ViewStyle;
  viewAllText: TextStyle;
  item: ViewStyle;
  itemLabel: ViewStyle;
  itemImage: ImageStyle;
  itemFirst: ViewStyle;
  itemLast: ViewStyle;
}

export const styles: Styles = StyleSheet.create({
  wrapper: {
    marginBottom: 40
  },
  header: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    paddingHorizontal: 16,
    marginBottom: 16,
    flex: 1
  },
  viewAllBtn: {
    height: 23
  },
  viewAllText: {
    fontSize: 11,
    paddingLeft: 0,
    paddingRight: 0
  },
  item: {
    width: 148,
    marginHorizontal: 12
  },
  itemLabel: {
    position: 'absolute',
    left: 9,
    top: 8
  },
  itemFirst: {
    marginLeft: 16
  },
  itemLast: {
    marginRight: 16
  },
  itemImage: {
    width: 148,
    height: 184,
    borderRadius: 8,
    marginBottom: 7
  }
} as Styles) as Styles;
