import { ViewStyle, ImageStyle, TextStyle } from 'react-native';
import StyleSheet from 'react-native-extended-stylesheet';

interface Styles {
  card: ViewStyle;
  image: ImageStyle;
  title: TextStyle;
  label: ViewStyle;
}

export const styles: Styles = StyleSheet.create({
  card: {
    position: 'relative'
  },
  image: {
    borderRadius: 8,
    overflow: 'hidden',
    marginBottom: 5,
    position: 'relative'
  },
  title: {
    marginTop: 5
  },
  label: {
    position: 'absolute',
    left: 9,
    top: 8
  }
} as Styles) as Styles;
