import React, { useMemo } from 'react';
import noop from 'lodash/noop';
import { TouchableOpacity, ViewStyle } from 'react-native';
import { H3, View, Badge, Text } from 'native-base';
import { Image } from '../image/image.component';
import { styles } from './product-card.styles';

interface ProductCardProps {
  title: string;
  image: { uri: string; width: number; height: number };
  label?: string;
  style?: ViewStyle;
  onPress?: () => void;
}

export function ProductCard({
  title,
  image,
  style,
  label,
  onPress = noop
}: ProductCardProps): JSX.Element {
  const lbl = useMemo(() => {
    return label ? (
      <Badge style={styles.label}>
        <Text>{label}</Text>
      </Badge>
    ) : null;
  }, [label]);

  return (
    <TouchableOpacity style={[styles.card, style]} onPress={onPress}>
      <View style={styles.image}>
        <Image
          uri={image.uri}
          style={{ width: image.width, height: image.height }}
        />
        {lbl}
      </View>
      <H3 style={styles.title}>{title}</H3>
    </TouchableOpacity>
  );
}
