import { ViewStyle, ImageStyle, TextStyle } from 'react-native';
import StyleSheet from 'react-native-extended-stylesheet';

interface Styles {
  image: ImageStyle;
  content: ViewStyle;
  imageWrapper: ViewStyle;
  body: ViewStyle;
  title: TextStyle;
  col: ViewStyle;
  characteristic: TextStyle;
}

export const styles: Styles = StyleSheet.create({
  image: {
    width: 104,
    height: 104
  },
  content: {
    paddingLeft: 0,
    paddingTop: 0,
    paddingBottom: 0,
    paddingRight: 0,
    margin: 0,
    borderRadius: 8,
    overflow: 'hidden'
  },
  imageWrapper: {
    width: 104,
    height: 104,
    flex: 0
  },
  body: {
    flex: 1,
    paddingLeft: 11,
    paddingTop: 11,
    paddingBottom: 18,
    paddingRight: 15,
    alignSelf: 'stretch'
  },
  title: {
    paddingBottom: 6
  },
  col: {
    display: 'flex',
    flexDirection: 'row'
  },
  characteristic: {
    fontSize: 13
  }
} as Styles) as Styles;
