import React from 'react';
import noop from 'lodash/noop';
import { Image } from 'react-native';
import { useTranslation } from 'react-i18next';
import { Card, CardItem, View, H3, Grid, Row, Col, Text } from 'native-base';
import { styles } from './catalog-card.styles';

interface Characteristic {
  label: string;
  value: string;
}

interface CatalogCardProps {
  title: string;
  image: string;
  characteristics?: Characteristic[];
  onPress?: () => void;
}

export function CatalogCard({
  title,
  image,
  characteristics = [],
  onPress = noop
}: CatalogCardProps): JSX.Element {
  const { t } = useTranslation();

  return (
    <Card>
      <CardItem button style={styles.content} onPress={onPress}>
        <View style={styles.imageWrapper}>
          <Image source={{ uri: image }} style={styles.image} />
        </View>
        <View style={styles.body}>
          <H3 style={styles.title}>{title}</H3>
          <Grid>
            <Row>
              {characteristics.map(
                ({ label, value }): JSX.Element => {
                  return (
                    <Col key={label} style={styles.col}>
                      <Text note style={styles.characteristic}>
                        {t('card.label', { label })}
                      </Text>
                      <Text> </Text>
                      <Text style={styles.characteristic}>{value}</Text>
                    </Col>
                  );
                }
              )}
            </Row>
          </Grid>
        </View>
      </CardItem>
    </Card>
  );
}
