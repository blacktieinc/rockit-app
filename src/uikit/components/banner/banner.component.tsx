import React from 'react';
import LinearGradient from 'react-native-linear-gradient';
import { View, H1 } from 'native-base';
import { Image } from '../image/image.component';
import { variable } from '../../theme/variables/fively';
import { styles } from './banner.styles';

interface BannerProps<T> {
  item: T;
  titleExtractor: (item: T) => string;
  wallpaperExtractor: (item: T) => string;
  logoExtractor?: (item: T) => string;
  height?: number;
}

export function Banner<T>({
  item,
  titleExtractor,
  wallpaperExtractor,
  logoExtractor,
  height = variable.deviceWidth * 0.57
}: BannerProps<T>): JSX.Element {
  const logo = logoExtractor && (
    <View style={styles.logoWrapper}>
      <Image style={styles.logo} uri={logoExtractor(item)} />
    </View>
  );

  return (
    <View style={[styles.banner, { height }]}>
      <Image
        style={{ ...styles.wallpaper, height }}
        uri={wallpaperExtractor(item)}
      />
      {logo}
      <LinearGradient
        colors={['rgba(0, 0, 0, 0)', 'rgb(0, 0, 0)']}
        style={[styles.title, logoExtractor && styles.titleWithLogo]}
      >
        <H1 style={styles.titleText}>{titleExtractor(item)}</H1>
      </LinearGradient>
    </View>
  );
}
