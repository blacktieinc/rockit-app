import { ViewStyle, ImageStyle, TextStyle } from 'react-native';
import StyleSheet from 'react-native-extended-stylesheet';
import { variable } from '../../theme/variables/fively';

interface Styles {
  banner: ViewStyle;
  wallpaper: ImageStyle;
  logoWrapper: ViewStyle;
  logo: ImageStyle;
  title: ViewStyle;
  titleWithLogo: ViewStyle;
  titleText: TextStyle;
}

export const styles: Styles = StyleSheet.create({
  banner: {
    width: variable.deviceWidth,
    height: variable.deviceWidth * 0.57,
    marginBottom: 37,
    position: 'relative'
  },
  wallpaper: {
    width: variable.deviceWidth,
    height: variable.deviceWidth * 0.57,
    position: 'relative',
    zIndex: 1
  },
  logoWrapper: {
    width: 76,
    height: 76,
    position: 'absolute',
    zIndex: 3,
    right: 16,
    bottom: -26,
    borderRadius: 38,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 4
    },
    shadowOpacity: 0.08,
    shadowRadius: 4,
    borderWidth: 2,
    borderColor: 'white'
  },
  logo: {
    width: 72,
    height: 72,
    borderRadius: 38
  },
  title: {
    position: 'absolute',
    bottom: 0,
    left: 0,
    right: 0,
    zIndex: 2,
    paddingHorizontal: 16,
    paddingBottom: 26,
    paddingTop: 40
  },
  titleWithLogo: {
    paddingRight: 70
  },
  titleText: {
    color: 'white'
  }
} as Styles) as Styles;
