import React from 'react';
import { ViewStyle, TouchableOpacity } from 'react-native';
import ViewMoreText from 'react-native-view-more-text';
import { Text, View } from 'native-base';
import { useTranslation } from 'react-i18next';
import {
  ExpandableTextHeading,
  ExpandableTextHeadingLayout
} from './heading/heading.component';
import { styles } from './expandable-text.styles';

interface ExpandableTextProps {
  text: string;
  title?: string;
  style?: ViewStyle;
  heading?: ExpandableTextHeadingLayout;
  transparent?: boolean;
}

export function ExpandableText({
  text,
  title,
  style,
  transparent,
  heading = 'h1'
}: ExpandableTextProps): JSX.Element {
  const { t } = useTranslation();

  const head = title && (
    <ExpandableTextHeading layout={heading}>{title}</ExpandableTextHeading>
  );

  const moreLessBtn = (label: string): ((onPress: () => void) => void) => {
    return function Button(onPress: () => void): JSX.Element {
      return (
        <TouchableOpacity onPress={onPress}>
          <Text style={styles.viewBtn}>{label}</Text>
        </TouchableOpacity>
      );
    };
  };

  return (
    <View style={style}>
      {head}
      <View style={[styles.content, transparent && styles.contentTransparent]}>
        <ViewMoreText
          numberOfLines={5}
          textStyle={styles.text}
          renderViewMore={moreLessBtn(t('buttons.viewMore'))}
          renderViewLess={moreLessBtn(t('buttons.viewLess'))}
        >
          <Text>{text}</Text>
        </ViewMoreText>
      </View>
    </View>
  );
}
