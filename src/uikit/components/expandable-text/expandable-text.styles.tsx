import { ViewStyle, TextStyle } from 'react-native';
import StyleSheet from 'react-native-extended-stylesheet';
import { variable } from '../../theme/variables/fively';

interface Styles {
  content: ViewStyle;
  contentTransparent: ViewStyle;
  heading: TextStyle;
  text: TextStyle;
  viewBtn: TextStyle;
}

export const styles: Styles = StyleSheet.create({
  heading: {
    marginBottom: 16
  },
  content: {
    backgroundColor: 'white',
    paddingLeft: 24,
    paddingRight: 20,
    paddingTop: 24,
    paddingBottom: 16,
    borderRadius: 8,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 1
    },
    shadowOpacity: 0.05,
    shadowRadius: 25
  },
  contentTransparent: {
    backgroundColor: 'transparent',
    shadowOpacity: 0,
    paddingLeft: 0,
    paddingRight: 0,
    paddingTop: 0,
    paddingBottom: 0
  },
  text: {
    fontSize: 14,
    lineHeight: 24,
    letterSpacing: -0.15,
    color: variable.defaultTextColor
  },
  viewBtn: {
    textAlign: 'right',
    color: variable.brandPrimary,
    lineHeight: 20,
    fontWeight: '500',
    marginTop: 8
  }
} as Styles) as Styles;
