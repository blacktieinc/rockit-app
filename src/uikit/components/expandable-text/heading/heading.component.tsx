import React, { PropsWithChildren } from 'react';
import { H1, H2, H3 } from 'native-base';
import { styles } from './heading.styles';

export type ExpandableTextHeadingLayout = 'h1' | 'h2' | 'h3';

interface ExpandableTextHeadingProps {
  layout: ExpandableTextHeadingLayout;
}

export function ExpandableTextHeading({
  children,
  layout
}: PropsWithChildren<ExpandableTextHeadingProps>): JSX.Element {
  if (layout === 'h1') {
    return <H1 style={styles.h1}>{children}</H1>;
  }
  if (layout === 'h2') {
    return <H2 style={styles.h2}>{children}</H2>;
  }
  if (layout === 'h3') {
    return <H3 style={styles.h3}>{children}</H3>;
  }
  return <></>;
}
