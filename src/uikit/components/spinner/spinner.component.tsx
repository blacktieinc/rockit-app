import React from 'react';
import { ViewStyle } from 'react-native';
import { View, Spinner as NbSpinner } from 'native-base';
import { variable } from '../../theme/variables/fively';
import { styles } from './spinner.styles';

interface SpinnerProps {
  center?: boolean;
  padder?: boolean;
  size?: 'small' | 'large';
  style?: ViewStyle;
  fill?: boolean;
  show?: boolean;
  backdrop?: boolean;
}

export function Spinner({
  center,
  padder,
  size,
  style,
  fill,
  backdrop,
  show = true
}: SpinnerProps): JSX.Element {
  return show ? (
    <View
      style={[
        styles.wrapper,
        center ? styles.centered : {},
        padder ? styles.padder : {},
        fill ? styles.fill : {},
        backdrop ? styles.backdrop : {},
        style
      ]}
    >
      <NbSpinner
        size={size}
        color={variable.textColor}
        style={styles.spinner}
      />
    </View>
  ) : null;
}
