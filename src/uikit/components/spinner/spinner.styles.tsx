import { ViewStyle } from 'react-native';
import StyleSheet from 'react-native-extended-stylesheet';
import { variable } from '../../theme';

interface Styles {
  spinner: ViewStyle;
  wrapper: ViewStyle;
  centered: ViewStyle;
  fill: ViewStyle;
  padder: ViewStyle;
  backdrop: ViewStyle;
}

export const styles: Styles = StyleSheet.create({
  spinner: {
    flex: 1
  },
  wrapper: {
    flex: 1,
    display: 'flex',
    flexDirection: 'row'
  },
  centered: {
    justifyContent: 'center',
    alignItems: 'center'
  },
  backdrop: {
    position: 'absolute',
    zIndex: 9999,
    backgroundColor: 'rgba(249,249,249, 0.7)'
  },
  fill: {
    width: variable.deviceWidth,
    height: variable.deviceHeight
  },
  padder: {
    padding: 13
  }
} as Styles) as Styles;
