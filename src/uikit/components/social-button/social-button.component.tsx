import React, { useMemo, useCallback } from 'react';
import { ViewStyle, Linking } from 'react-native';
import { Button } from 'native-base';
import { IconName, Icon } from '../icon/icon.component';
import { styles } from './social-button.styles';

interface SocialButtonProps {
  type: string;
  href: string;
  style?: ViewStyle;
}

const iconMap: Record<string, IconName> = {
  twitter: 'twitter',
  facebook: 'facebook',
  instagram: 'instagram',
  apple: 'applemusic',
  google: 'googleplay',
  youtube: 'youtube',
  telegram: 'telegram'
};

export function SocialButton({
  type,
  href,
  style
}: SocialButtonProps): JSX.Element {
  const icon = useMemo((): IconName => iconMap[type], [type]);

  const handleOnPress = useCallback(() => {
    Linking.openURL(href);
  }, [href]);

  return (
    <Button
      rounded
      large
      iconLeft
      style={[styles[icon], style]}
      onPress={handleOnPress}
    >
      <Icon name={icon} size={26} color='#fff' />
    </Button>
  );
}
