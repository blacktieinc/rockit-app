import { ViewStyle } from 'react-native';
import StyleSheet from 'react-native-extended-stylesheet';
import { IconName } from '@uikit/components';

type Styles = Partial<Record<IconName, ViewStyle>>;

const button = (color: string): ViewStyle => ({
  backgroundColor: color,
  width: 52,
  justifyContent: 'center',
  shadowColor: color,
  shadowOffset: {
    width: 0,
    height: 4
  },
  shadowOpacity: 0.08,
  shadowRadius: 4
});

export const styles: Styles = StyleSheet.create({
  facebook: button('#4172B8'),
  twitter: button('#1DA1F2'),
  instagram: button('#E4405F'),
  youtube: button('#FF0000'),
  applemusic: button('#000000'),
  googleplay: button('#607D8B'),
  telegram: button('#2CA5E0')
} as Styles);
