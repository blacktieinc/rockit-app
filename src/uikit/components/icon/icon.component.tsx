import React from 'react';
import Roqet from './icon.template';

export type IconName =
  | 'home'
  | 'home-fill'
  | 'profile'
  | 'profile-fill'
  | 'heart'
  | 'bag'
  | 'bag-fill'
  | 'chevron-left'
  | 'twitter'
  | 'facebook'
  | 'instagram'
  | 'youtube'
  | 'googleplay'
  | 'applemusic'
  | 'telegram'
  | 'close'
  | 'location'
  | 'arrow-right';

interface IconProps {
  name: IconName;
  size?: number;
  color?: string;
}

export function Icon({
  name,
  size = 20,
  color = '#222'
}: IconProps): JSX.Element {
  return <Roqet name={name} size={size} color={color} />;
}
