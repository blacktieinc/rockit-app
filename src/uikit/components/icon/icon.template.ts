/**
 * RockIt icon set component.
 * Usage: <RockIt name="icon-name" size={20} color="#4F8EF7" />
 */

import createIconSet from 'react-native-vector-icons/lib/create-icon-set';
const glyphMap = {
  "applemusic": 61697,
  "arrow-right": 61698,
  "bag-fill": 61699,
  "bag": 61700,
  "chevron-left": 61701,
  "close": 61702,
  "facebook": 61703,
  "googleplay": 61704,
  "heart": 61705,
  "home-fill": 61706,
  "home": 61707,
  "instagram": 61708,
  "location": 61709,
  "profile-fill": 61710,
  "profile": 61711,
  "telegram": 61712,
  "twitter": 61713,
  "youtube": 61714
};

const iconSet = createIconSet(glyphMap, 'RockIt', 'RockIt.ttf');

export default iconSet;

export const Button = iconSet.Button;
export const TabBarItem = iconSet.TabBarItem;
export const TabBarItemIOS = iconSet.TabBarItemIOS;
export const ToolbarAndroid = iconSet.ToolbarAndroid;
export const getImageSource = iconSet.getImageSource;
