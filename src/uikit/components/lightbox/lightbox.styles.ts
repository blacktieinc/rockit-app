import { ViewStyle } from 'react-native';
import StyleSheet from 'react-native-extended-stylesheet';
import { variable } from '@uikit/theme';

interface Styles {
  root: ViewStyle;
  container: ViewStyle;
  top: ViewStyle;
  content: ViewStyle;
}

export const styles: Styles = StyleSheet.create({
  root: {
    position: 'absolute',
    left: 0,
    top: 0,
    right: 0,
    bottom: 0,
    backgroundColor: 'rgba(0, 0, 0, 0.2)'
  },
  container: {
    position: 'absolute',
    bottom: 0,
    left: 0,
    right: 0,
    backgroundColor: variable.containerBgColor,
    borderTopRightRadius: 34,
    borderTopLeftRadius: 34,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: -4
    },
    shadowOpacity: 0.08,
    shadowRadius: 30,
    display: 'flex'
  },
  top: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'flex-end',
    paddingHorizontal: 24,
    paddingTop: 8,
    flex: 0
  },
  content: {
    flex: 1
  }
} as Styles) as Styles;
