import React, { PropsWithChildren, useEffect, useCallback } from 'react';
import hexToRgba from 'hex-to-rgba';
import { Animated } from 'react-native';
import { View, Button } from 'native-base';
import { Actions } from 'react-native-router-flux';
import { variable } from '@uikit/theme';
import { Icon } from '@uikit/components';
import { styles } from './lightbox.styles';

export function Lightbox({ children }: PropsWithChildren<{}>): JSX.Element {
  const value = new Animated.Value(0);

  const backgroundColor = value.interpolate({
    inputRange: [0, 1],
    outputRange: [hexToRgba('#000000', 0), hexToRgba('#000000', 0.2)],
    extrapolate: 'clamp'
  });

  const bottom = value.interpolate({
    inputRange: [0, 1],
    outputRange: [-variable.deviceHeight, 0],
    extrapolate: 'clamp'
  });

  const handleClose = useCallback(() => {
    Animated.timing(value, {
      duration: 300,
      toValue: 0
    }).start(Actions.pop);
  }, [value]);

  useEffect(() => {
    Animated.timing(value, {
      duration: 300,
      toValue: 1
    }).start();
  }, [value]);

  return (
    <Animated.View style={[styles.root, { backgroundColor }]}>
      <Animated.View style={[styles.container, { bottom }]}>
        <View style={styles.top}>
          <Button dark transparent onPress={handleClose}>
            <Icon name='close' color='#9B9B9B' />
          </Button>
        </View>
        <View style={styles.content}>{children}</View>
      </Animated.View>
    </Animated.View>
  );
}
