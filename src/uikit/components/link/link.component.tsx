import React, { PropsWithChildren, useCallback } from 'react';
import { TouchableOpacity, Linking } from 'react-native';
import { Text } from 'native-base';
import { styles } from './link.styles';

interface LinkProps {
  href: string;
  small?: boolean;
}

export function Link({
  children,
  href,
  small
}: PropsWithChildren<LinkProps>): JSX.Element {
  const handelOnPress = useCallback(() => {
    Linking.openURL(href);
  }, [href]);

  return (
    <TouchableOpacity onPress={handelOnPress}>
      <Text numberOfLines={1} style={[styles.text]}>
        {children}
      </Text>
    </TouchableOpacity>
  );
}
