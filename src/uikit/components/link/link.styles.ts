import { ViewStyle, TextStyle } from 'react-native';
import StyleSheet from 'react-native-extended-stylesheet';

interface Styles {
  button: ViewStyle;
  text: TextStyle;
  small: TextStyle;
}

export const styles: Styles = StyleSheet.create({
  button: {},
  text: {
    paddingLeft: 0,
    paddingRight: 0
  }
} as Styles) as Styles;
