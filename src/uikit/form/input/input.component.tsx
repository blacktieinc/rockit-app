import get from 'lodash/get';
import React, { useCallback } from 'react';
import { KeyboardType } from 'react-native';
import { Field, FieldProps } from 'formik';
import { Item, Label, View, Text, Input as BaseInput } from 'native-base';
import { styles } from './input.styles';

interface InputProps {
  label: string;
  name: string;
  type?: KeyboardType;
  secure?: boolean;
}

export function Input({
  label,
  name,
  secure,
  type = 'default'
}: InputProps): JSX.Element {
  const renderError = useCallback((invalid: boolean, error: string) => {
    return invalid ? <Text style={styles.error}>{error}</Text> : null;
  }, []);

  const renderInput = useCallback(
    ({
      field: { value },
      form: { setFieldValue, setFieldTouched, errors, touched }
    }: FieldProps) => {
      const inputError = get(errors, name);
      const inputTouched = get(touched, name);
      const inputInvalid = inputError && inputTouched;
      return (
        <View style={[styles.wrapper, inputInvalid && styles.wrapperInvalid]}>
          <Item floatingLabel>
            <Label>{label}</Label>
            <BaseInput
              value={value}
              onChangeText={(val): void => setFieldValue(name, val)}
              onBlur={(): void => setFieldTouched(name, true)}
              secureTextEntry={secure}
              keyboardType={type}
            />
          </Item>
          {renderError(inputInvalid as boolean, inputError as string)}
        </View>
      );
    },
    [label, name, renderError, secure, type]
  );

  return <Field name={name}>{renderInput}</Field>;
}
