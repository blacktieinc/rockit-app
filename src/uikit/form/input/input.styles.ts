import { TextStyle, ViewStyle } from 'react-native';
import StyleSheet from 'react-native-extended-stylesheet';

interface Styles {
  wrapper: ViewStyle;
  wrapperInvalid: ViewStyle;
  error: TextStyle;
}

export const styles: Styles = StyleSheet.create({
  wrapper: {
    backgroundColor: '#fff',
    marginBottom: 24,
    paddingVertical: 3,
    borderRadius: 4,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 1
    },
    shadowOpacity: 0.05,
    shadowRadius: 8,
    borderWidth: 1,
    borderColor: '#fff',
    position: 'relative'
  },
  wrapperInvalid: {
    borderColor: '#F01F0E'
  },
  error: {
    position: 'absolute',
    bottom: -18,
    fontSize: 11,
    lineHeight: 11,
    color: '#F01F0E',
    paddingLeft: 20
  }
} as Styles) as Styles;
