import React, { PropsWithChildren } from 'react';
import { Form as BaseForm } from 'native-base';
import { Input } from './input/input.component';

export function Form({ children }: PropsWithChildren<{}>): JSX.Element {
  return <BaseForm>{children}</BaseForm>;
}

Form.Input = Input;
