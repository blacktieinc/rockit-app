import React from 'react';
import {
  Actions,
  Scene,
  Modal,
  Tabs,
  Lightbox,
  Overlay,
  Stack
} from 'react-native-router-flux';

import { SplashContainer } from '@screens/splash/splash.container';
import { HomeContainer } from '@screens/root/home/home.container';
import { ProfileContainer } from '@screens/root/profile/profile.container';
import { FestivalGQL } from '@screens/festival/festival.component';
import { LineupGQL } from '@screens/lineup/lineup.component';
import { ArtistGQL } from '@screens/artist/artist.component';

import { MapLightbox } from '@lightboxes/map/map.component';
import { FestivalsContainer } from '@/screens/root/festivals/festivals.container';
import { ForgotPasswordContainer } from '@/screens/forgot-password/forgot-password.container';
import { SignUpContainer } from '@/screens/singup/signup.container';
import { SignInContainer } from '@/screens/signin/signin.container';

import { TabBar } from './app.tabbar';

export const Routes = Actions.create(
  <Overlay>
    <Modal key='modal' hideNavBar>
      <Lightbox key='lightbox'>
        <Stack key='screens' hideNavBar>
          <Scene hideNavBar key='splash' component={SplashContainer} />
          <Scene hideNavBar key='auth'>
            <Scene hideNavBar key='auth.signin' component={SignInContainer} />
            <Scene hideNavBar key='auth.signup' component={SignUpContainer} />
            <Scene
              hideNavBar
              key='auth.password'
              component={ForgotPasswordContainer}
            />
          </Scene>
          <Tabs key='root' tabBarComponent={TabBar}>
            <Scene hideNavBar key='root.home' component={HomeContainer} />
            <Scene
              hideNavBar
              key='root.festivals'
              component={FestivalsContainer}
            />
            <Scene hideNavBar key='root.profile' component={ProfileContainer} />
          </Tabs>
          <Scene hideNavBar key='festival' component={FestivalGQL} />
          <Scene hideNavBar key='lineup' component={LineupGQL} />
          <Scene hideNavBar key='artist' component={ArtistGQL} />
        </Stack>
        <Scene hideNavBar key='map' component={MapLightbox} />
      </Lightbox>
    </Modal>
  </Overlay>
);
