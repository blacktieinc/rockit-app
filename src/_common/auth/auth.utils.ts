import env from 'react-native-config';
import {
  GraphRequest,
  GraphRequestManager,
  LoginManager,
  LoginResult
} from 'react-native-fbsdk';
import {
  GoogleSignin,
  User as GoogleUser
} from '@react-native-community/google-signin';

GoogleSignin.configure({
  webClientId: env.GOOGLE_OAUTH_CLIEN_ID
});

export function loginWithFacebook(): Promise<LoginResult> {
  return LoginManager.logInWithPermissions(['public_profile', 'email']);
}

export function getFacebookUser(): Promise<FB.Me> {
  return new Promise((resolve, reject) => {
    const infoRequest = new GraphRequest(
      '/me',
      {
        parameters: {
          fields: {
            string: 'id,first_name,last_name,email,picture.type(large)'
          }
        }
      },
      (error, result): void => {
        if (error) reject(error);
        else resolve(result as FB.Me);
      }
    );
    new GraphRequestManager().addRequest(infoRequest).start();
  });
}

export function loginWithGoogle(): Promise<GoogleUser> {
  return GoogleSignin.signIn();
}
