/* eslint-disable @typescript-eslint/no-var-requires */
import React, { PropsWithChildren, useMemo, useCallback } from 'react';
import { Image } from 'react-native';
import {
  View,
  Text,
  Button,
  Container,
  Header,
  Content,
  Left,
  Body
} from 'native-base';
import { Actions } from 'react-native-router-flux';
import { Headline, Icon } from '@uikit/components';
import { useSignInFacebook, useSignInGoogle } from '@shared/auth';
import { styles } from './auth.styles';

const google = require('../../assets/google.png');
const facebook = require('../../assets/facebook.png');

export interface AuthProps {
  headline: string;
  back?: boolean;
  actions?: string;
  logInWithSicialNetwork: (key: string) => void;
}

export function Auth({
  back,
  headline,
  actions,
  children,
  logInWithSicialNetwork
}: PropsWithChildren<AuthProps>): JSX.Element {
  const [signInWithFacebook] = useSignInFacebook(() => {
    logInWithSicialNetwork('Facebook');
  });

  const [signInWithGoogle] = useSignInGoogle(() => {
    logInWithSicialNetwork('Google');
  });

  const handlePop = useCallback(() => Actions.pop(), []);

  const pop = useMemo(() => {
    return back ? (
      <Left>
        <Button transparent onPress={handlePop}>
          <Icon name='chevron-left' />
        </Button>
      </Left>
    ) : null;
  }, [back, handlePop]);

  const bottom = useMemo(() => {
    return actions ? (
      <View style={styles.bottom}>
        <Text style={styles.label}>{actions}</Text>
        <View style={styles.actions}>
          <Button light style={styles.button} onPress={signInWithGoogle}>
            <Image source={google} style={styles.icon} />
          </Button>
          <Button light style={styles.button} onPress={signInWithFacebook}>
            <Image source={facebook} style={styles.icon} />
          </Button>
        </View>
      </View>
    ) : null;
  }, [actions, signInWithFacebook, signInWithGoogle]);

  return (
    <Container>
      <Header transparent>
        {pop}
        <Body />
      </Header>
      <Content
        style={styles.content}
        contentContainerStyle={styles.contentContainer}
      >
        <Headline style={styles.headline}>{headline}</Headline>
        <View style={styles.container}>
          <View style={styles.children}>{children}</View>
        </View>
        {bottom}
      </Content>
    </Container>
  );
}
