import { ViewStyle, ImageStyle, TextStyle } from 'react-native';
import StyleSheet from 'react-native-extended-stylesheet';
import { variable } from '@uikit/theme';

interface Styles {
  headline: ViewStyle;
  contentContainer: ViewStyle;
  container: ViewStyle;
  content: ViewStyle;
  children: ViewStyle;
  bottom: ViewStyle;
  actions: ViewStyle;
  label: TextStyle;
  icon: ImageStyle;
  button: ViewStyle;
  formActions: ViewStyle;
  formActionText: TextStyle;
}

export const styles: Styles = StyleSheet.create({
  headline: {
    flex: 0,
    marginBottom: 73
  },
  contentContainer: {
    flex: 1
  },
  container: {
    flex: 1
  },
  content: {
    flex: 1,
    paddingHorizontal: 16
  },
  children: {
    flex: 1
  },
  bottom: {
    flex: 0,
    paddingBottom: 12
  },
  actions: {
    flexDirection: 'row',
    justifyContent: 'center'
  },
  label: {
    textAlign: 'center',
    marginBottom: 12,
    fontSize: 14
  },
  button: {
    width: 92,
    height: 64,
    justifyContent: 'center',
    marginHorizontal: 8,
    borderRadius: 24,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 1
    },
    shadowOpacity: 0.05,
    shadowRadius: 8
  },
  icon: {
    height: 24,
    width: 24
  },
  formActions: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginTop: -10,
    marginBottom: 32,
    paddingHorizontal: 4
  },
  formActionText: {
    fontWeight: '500',
    paddingLeft: 0
  }
} as Styles) as Styles;
