import { connect } from 'react-redux';
import { UserActions } from '@store/user';
import { Auth, AuthProps } from './auth.component';

const mapDispatchToProps = (dispatch): Partial<AuthProps> => ({
  logInWithSicialNetwork: (network: string): void =>
    dispatch(new UserActions.LogInWithSocialNetwork(network))
});

export const AuthContainer = connect(
  null,
  mapDispatchToProps
)(Auth);
