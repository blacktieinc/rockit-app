declare namespace ARGS {
  interface Paging {
    page: number;
    perPage: number;
    sort?: string;
  }
}
