declare namespace DTO {
  interface UserName {
    first: string;
    last: string;
  }
  interface User {
    id: string;
    name: UserName;
    email: string;
    role: string;
    facebookId: string;
    googleId: string;
    auth: AuthType;
  }
}
