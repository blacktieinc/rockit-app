declare namespace DTO {
  interface Event {
    id: string;
    title: string;
    slug: string;
    logo: string;
    wallpaper: string;
    startDateTime: number;
    endDateTime: number;
    summary: string;
    homepage: string;
    tickets: string;
    organizer: Organizer;
    location: Location;
  }
}
