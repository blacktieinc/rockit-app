declare namespace DTO {
  type AuthType = 'default' | 'facebook' | 'google';

  interface Token {
    token: string;
  }

  interface Credentials {
    email: string;
    password: string;
  }

  interface SocialSignIn {
    id: string;
    email: string;
    firstName: string;
    lastName: string;
  }
}
