declare namespace DTO {
  interface Geometry {
    lat: number;
    lng: number;
  }
  interface Location {
    address: string;
    placeId: string;
    geometry: Geometry;
  }
}
