declare namespace DTO {
  interface Organizer {
    id: string;
    title: string;
    slug: string;
    logo: string;
    wallpaper: string;
    summary: string;
    social: DTO.Social;
  }
}
