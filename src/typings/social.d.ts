declare namespace DTO {
  interface Social {
    facebook?: string;
    instagram?: string;
    twitter?: string;
    telegram?: string;
    youtube?: string;
  }
}
