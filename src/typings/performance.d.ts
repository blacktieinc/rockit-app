declare namespace DTO {
  interface Performance {
    id: string;
    headliner: boolean;
    artist: DTO.Artist;
    event: DTO.Event;
    createdAt: number;
    updatedAt: number;
  }
}
