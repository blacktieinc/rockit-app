declare namespace DTO {
  interface Paged<T> {
    total: number;
    results: T[];
    currentPage: number;
    totalPages: number;
    pages: number[];
    previous: number;
    next: number;
    first: number;
    last: number;
  }
}
