declare namespace FB {
  interface Me {
    email: string;
    first_name: string;
    id: string;
    last_name: string;
    picture: {
      data: {
        height: number;
        is_silhouette: boolean;
        url: string;
        width: number;
      };
    };
  }
}
