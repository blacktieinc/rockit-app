declare namespace DTO {
  interface Artist {
    id: string;
    mbid: string;
    title: string;
    slug: string;
    picture: string;
    homepage: string;
    country: string[];
    socialNetworks: ExternalResource[];
    purchaseForDownload: ExternalResource[];
    youtube: ExternalResource[];
    content: string;
    genres: string[];
  }
}
