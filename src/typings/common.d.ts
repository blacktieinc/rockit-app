declare namespace DTO {
  interface ExternalResource {
    type: string;
    url: string;
  }
}
