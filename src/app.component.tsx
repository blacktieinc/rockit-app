import React from 'react';
import StyleSheet from 'react-native-extended-stylesheet';
import { Root } from 'native-base';
import { Provider, connect } from 'react-redux';
import { Router } from 'react-native-router-flux';
import { store } from '@store/app.store';
import { Theme } from '@uikit/theme';
import { AppClient } from '@shared/client';
import { ApolloProvider } from '@apollo/react-hooks';
import { Routes } from './app.routes';

const ReduxRouter = connect()(Router);

StyleSheet.build();

export function App(): JSX.Element {
  return (
    <Theme>
      <Root>
        <ApolloProvider client={AppClient}>
          <Provider store={store}>
            <ReduxRouter scenes={Routes} />
          </Provider>
        </ApolloProvider>
      </Root>
    </Theme>
  );
}
