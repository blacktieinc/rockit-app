import React from 'react';
import get from 'lodash/get';
import { Footer, FooterTab, Button } from 'native-base';
import { Actions } from 'react-native-router-flux';
import { Icon, IconName } from '@uikit/components';

interface FooterItem {
  icon: IconName;
}

const TabsMap: Record<string, IconName> = {
  'root.home': 'home',
  'root.profile': 'profile',
  'root.festivals': 'bag'
};

const ColorMap: Record<string, string> = {
  false: '#9B9B9B',
  true: '#DB3022'
};

export function TabBar(props: any): JSX.Element {
  const routes = get(props, 'navigation.state.routes', []);
  const current = get(props, 'navigation.state.index', 0);

  return (
    <Footer>
      <FooterTab>
        {routes.map(
          ({ key }, index): JSX.Element => {
            return (
              <Button key={key} onPress={(): void => Actions[key]()}>
                <Icon
                  name={`${get(TabsMap, key)}${
                    index === current ? '-fill' : ''
                  }`}
                  size={25}
                  color={ColorMap[(index === current).toString()]}
                />
              </Button>
            );
          }
        )}
      </FooterTab>
    </Footer>
  );
}
