import { connect } from 'react-redux';
import { UserActions, UserSelectors } from '@store/user';
import { AppState } from '@store/app.state';
import { Login, LoginProps } from './signin.component';

const mapStateToProps = (state: AppState): Partial<LoginProps> => ({
  logInInProgress: UserSelectors.loading(state)
});

const mapDispatchToProps = (dispatch): Partial<LoginProps> => ({
  logIn: (): void => dispatch(new UserActions.LogIn())
});

export const SignInContainer = connect(
  mapStateToProps,
  mapDispatchToProps
)(Login);
