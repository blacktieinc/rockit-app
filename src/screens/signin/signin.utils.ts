import { useTranslation } from 'react-i18next';
import { object, string, Schema } from 'yup';

export function useValidationSchema(): { validationSchema: Schema<{}> } {
  const { t } = useTranslation();

  const validationSchema = object().shape({
    email: string()
      .required(
        t('validation.required', {
          field: t('forms.email')
        })
      )
      .email(
        t('validation.email', {
          field: t('forms.email')
        })
      ),
    password: string().required(
      t('validation.required', {
        field: t('forms.password')
      })
    )
  });

  return { validationSchema };
}
