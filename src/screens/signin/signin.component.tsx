import React, { useCallback, useMemo } from 'react';
import { Actions } from 'react-native-router-flux';
import { useTranslation } from 'react-i18next';
import { Button, Text, View } from 'native-base';
import { Formik } from 'formik';
import { Form } from '@uikit/form';
import { Icon, Spinner } from '@uikit/components';
import { variable } from '@uikit/theme';
import { useSignIn } from '@shared/auth';
import { AuthContainer, styles } from '@common/auth';
import { useValidationSchema } from './signin.utils';

export interface LoginProps {
  logInInProgress: boolean;
  logIn: () => void;
}

export function Login({
  logIn,
  logInInProgress = false
}: LoginProps): JSX.Element {
  const { t } = useTranslation();
  const { validationSchema } = useValidationSchema();
  const [signIn, loading] = useSignIn(logIn);

  const handleLogIn = useCallback(
    (values: DTO.Credentials) => {
      signIn({
        ...values,
        email: values.email.toLowerCase()
      });
    },
    [signIn]
  );

  const form = useMemo(
    (): JSX.Element => (
      <Form>
        <Form.Input
          type='email-address'
          name='email'
          label={t('forms.email')}
        />
        <Form.Input secure name='password' label={t('forms.password')} />
      </Form>
    ),
    [t]
  );

  const actions = useMemo(
    (): JSX.Element => (
      <View style={styles.formActions}>
        <Button
          dark
          transparent
          small
          iconRight
          onPress={(): void => Actions.push('auth.signup')}
        >
          <Text style={styles.formActionText}>{t('buttons.signUp')}</Text>
          <Icon name='arrow-right' size={8} color={variable.brandPrimary} />
        </Button>
        <Button
          dark
          transparent
          small
          iconRight
          onPress={(): void => Actions.push('auth.password')}
        >
          <Text style={styles.formActionText}>
            {t('buttons.forgotYourPassword')}
          </Text>
          <Icon name='arrow-right' size={8} color={variable.brandPrimary} />
        </Button>
      </View>
    ),
    [t]
  );

  return (
    <>
      <Spinner show={loading || logInInProgress} fill center backdrop />
      <AuthContainer
        headline={t('navigation.login')}
        actions={t('forms.orLoginWithSocialSccount')}
      >
        <Formik
          isInitialValid={false}
          validationSchema={validationSchema}
          initialValues={{ email: '', password: '' }}
          onSubmit={(): void => {}}
        >
          {({ values, isValid }): JSX.Element => (
            <>
              {form}
              {actions}
              <Button
                primary
                rounded
                block
                disabled={!isValid}
                onPress={(): void => handleLogIn(values)}
              >
                <Text>{t('buttons.login').toUpperCase()}</Text>
              </Button>
            </>
          )}
        </Formik>
      </AuthContainer>
    </>
  );
}
