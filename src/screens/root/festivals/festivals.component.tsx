import React, { useMemo, useCallback } from 'react';
import moment from 'moment';
import get from 'lodash/get';
import merge from 'lodash/merge';
import { Actions } from 'react-native-router-flux';
import { ChildDataProps } from '@apollo/react-hoc';
import { FlatList, Animated } from 'react-native';
import { Container, Content, Title } from 'native-base';
import { useTranslation } from 'react-i18next';
import { CatalogCard, Header, Headline, Spinner } from '@uikit/components';
import { styles } from './festivals.styles';

export interface FestivalsResponse {
  getEvents: DTO.Paged<DTO.Event>;
}

export interface FestivalsVariables {
  page: number;
  perPage: number;
  width: number;
}

export type FestivalsProps = ChildDataProps<
  {},
  FestivalsResponse,
  FestivalsVariables
>;

export function Festivals({
  data: { getEvents, loading, fetchMore }
}: FestivalsProps): JSX.Element {
  const scrollY = new Animated.Value(0);
  const { t } = useTranslation();
  const results = useMemo(() => get(getEvents, 'results', []), [getEvents]);

  const renderItem = ({
    id,
    title,
    logo,
    startDateTime,
    endDateTime
  }: DTO.Event): JSX.Element => {
    return (
      <CatalogCard
        title={title}
        image={logo}
        onPress={(): void => Actions.push('festival', { id })}
        characteristics={[
          {
            label: t('forms.dates'),
            value: t('forms.dateRange', {
              start: moment(startDateTime).format('L'),
              end: moment(endDateTime).format('L')
            })
          }
        ]}
      />
    );
  };

  const handleOnEndReached = useCallback(() => {
    if (get(getEvents, 'next') && !loading) {
      fetchMore({
        variables: {
          page: getEvents.next
        },
        updateQuery: (prev, { fetchMoreResult }) => {
          return merge({}, prev, fetchMoreResult, {
            getEvents: {
              results: [
                ...prev.getEvents.results,
                ...fetchMoreResult.getEvents.results
              ]
            }
          });
        }
      });
    }
  }, [fetchMore, getEvents, loading]);

  return (
    <Container>
      <Header scrollY={scrollY}>
        <Header.Body scrollY={scrollY}>
          <Title>{t('navigation.festivals')}</Title>
        </Header.Body>
      </Header>
      <Content
        style={styles.content}
        contentContainerStyle={styles.contentContainer}
      >
        <FlatList
          data={results}
          // refreshing={refreshing}
          renderItem={({ item }): JSX.Element => renderItem(item)}
          keyExtractor={(item): string => item.id}
          // onRefresh={(): void => load(1)}
          style={styles.list}
          ListHeaderComponent={(): JSX.Element => (
            <Headline>{t('navigation.festivals')}</Headline>
          )}
          ListFooterComponent={(): JSX.Element => loading && <Spinner />}
          onScroll={Animated.event([
            { nativeEvent: { contentOffset: { y: scrollY } } }
          ])}
          onEndReached={handleOnEndReached}
        />
      </Content>
    </Container>
  );
}
