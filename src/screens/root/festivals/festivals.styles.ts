import { ViewStyle } from 'react-native';
import StyleSheet from 'react-native-extended-stylesheet';

interface Styles {
  content: ViewStyle;
  contentContainer: ViewStyle;
  list: ViewStyle;
}

export const styles: Styles = StyleSheet.create({
  content: {
    flex: 1
  },
  contentContainer: {
    flex: 1
  },
  list: {
    paddingHorizontal: 15
  }
} as Styles) as Styles;
