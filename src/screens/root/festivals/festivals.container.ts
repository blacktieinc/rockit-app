import { PixelRatio } from 'react-native';
import { graphql } from '@apollo/react-hoc';
import {
  Festivals,
  FestivalsProps,
  FestivalsVariables,
  FestivalsResponse
} from './festivals.component';
import { LOAD_QUERY } from './festivals.queries';

export const FestivalsContainer = graphql<
  {},
  FestivalsResponse,
  FestivalsVariables,
  FestivalsProps
>(LOAD_QUERY, {
  options: () => ({
    variables: {
      page: 1,
      perPage: 25,
      width: PixelRatio.getPixelSizeForLayoutSize(104)
    }
  })
})(Festivals);
