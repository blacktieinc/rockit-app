import gql from 'graphql-tag';

export const LOAD_QUERY = gql`
  query LoadFestivals($page: Int!, $perPage: Int!, $width: Float!) {
    getEvents(page: $page, perPage: $perPage) {
      currentPage
      next
      results {
        id
        title
        startDateTime
        endDateTime
        logo(width: $width)
      }
    }
  }
`;
