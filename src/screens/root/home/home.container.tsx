import React, { useCallback } from 'react';
import get from 'lodash/get';
import merge from 'lodash/merge';
import { PixelRatio } from 'react-native';
import { graphql, DataProps } from '@apollo/react-hoc';
import { variable } from '@uikit/theme';
import { Home } from './home.component';
import { LOAD_QUERY } from './home.queries';
import { styles } from './home.styles';

interface HomeResponse {
  getPerformanceFeed: DTO.Paged<DTO.Performance>;
}

interface HomeVariables {
  page: number;
  perPage: number;
  sort: string;
  logoW: number;
  pictureW: number;
  pictureH: number;
}

type HomeProps = DataProps<HomeResponse, HomeVariables>;

export const HomeContainer = graphql<
  {},
  HomeResponse,
  HomeVariables,
  HomeProps
>(LOAD_QUERY, {
  options: () => ({
    variables: {
      page: 1,
      perPage: 20,
      sort: '-createdAt',
      logoW: PixelRatio.getPixelSizeForLayoutSize(56),
      pictureW: PixelRatio.getPixelSizeForLayoutSize(styles.picture
        .width as number),
      pictureH: PixelRatio.getPixelSizeForLayoutSize(styles.picture
        .height as number)
    }
  })
})(
  ({
    data: { getPerformanceFeed, loading, fetchMore, error }
  }): JSX.Element => {
    const handleOnEndReached = useCallback(() => {
      if (get(getPerformanceFeed, 'next') && !loading) {
        fetchMore({
          variables: {
            page: getPerformanceFeed.next
          },
          updateQuery: (prev, { fetchMoreResult }) => {
            return merge({}, prev, fetchMoreResult, {
              getPerformanceFeed: {
                results: [
                  ...prev.getPerformanceFeed.results,
                  ...fetchMoreResult.getPerformanceFeed.results
                ]
              }
            });
          }
        });
      }
    }, [fetchMore, getPerformanceFeed, loading]);

    return (
      <Home
        loading={loading}
        data={get(getPerformanceFeed, 'results', [])}
        onEndReached={handleOnEndReached}
      />
    );
  }
);
