import { ViewStyle, ImageStyle } from 'react-native';
import StyleSheet from 'react-native-extended-stylesheet';
import { variable } from '@uikit/theme';

const pictureW = variable.deviceWidth - 32;
const pictureH = (pictureW / 16) * 9;

interface Styles {
  content: ViewStyle;
  contentContainer: ViewStyle;
  list: ViewStyle;
  picture: ImageStyle;
}

export const styles: Styles = StyleSheet.create({
  content: {
    flex: 1
  },
  contentContainer: {
    flex: 1
  },
  list: {
    paddingHorizontal: 15
  },
  picture: {
    width: pictureW,
    height: pictureH,
    borderBottomLeftRadius: 8,
    borderBottomRightRadius: 8
  }
} as Styles) as Styles;
