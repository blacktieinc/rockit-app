import gql from 'graphql-tag';

export const LOAD_QUERY = gql`
  query LoadPerformanceFeed(
    $page: Int!
    $perPage: Int!
    $sort: String
    $logoW: Float!
    $pictureW: Float!
    $pictureH: Float!
  ) {
    getPerformanceFeed(page: $page, perPage: $perPage, sort: $sort) {
      next
      results {
        id
        createdAt
        updatedAt
        artist {
          title
          picture(width: $pictureW, height: $pictureH, gravity: "faces")
        }
        event {
          title
          logo(width: $logoW)
        }
      }
    }
  }
`;
