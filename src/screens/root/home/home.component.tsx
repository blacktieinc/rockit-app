import React from 'react';
import { Animated } from 'react-native';
import { useTranslation } from 'react-i18next';
import {
  Text,
  Container,
  Content,
  Title,
  Card,
  CardItem,
  Left,
  Body,
  Thumbnail,
  H3
} from 'native-base';
import { Header, Headline, Spinner, Image } from '@uikit/components';
import { styles } from './home.styles';

interface HomeProps {
  loading: boolean;
  data: DTO.Performance[];
  onEndReached: () => void;
}

export function Home({ loading, data, onEndReached }: HomeProps): JSX.Element {
  const scrollY = new Animated.Value(0);
  const { t } = useTranslation();

  const renderItem = ({
    artist: { title: artistTitle, picture },
    event: { logo, title: festTitle }
  }: DTO.Performance): JSX.Element => {
    return (
      <Card>
        <CardItem>
          <Left>
            <Thumbnail source={{ uri: logo }} />
            <Body>
              <H3>{artistTitle}</H3>
              <Text note>{t('forms.atFestival', { festival: festTitle })}</Text>
            </Body>
          </Left>
        </CardItem>
        <CardItem cardBody>
          <Image uri={picture} style={styles.picture} />
        </CardItem>
      </Card>
    );
  };

  return (
    <Container>
      <Header scrollY={scrollY}>
        <Header.Body scrollY={scrollY}>
          <Title>{t('navigation.feed')}</Title>
        </Header.Body>
      </Header>
      <Content
        style={styles.content}
        contentContainerStyle={styles.contentContainer}
      >
        <Animated.FlatList
          data={data}
          // refreshing={refreshing}
          renderItem={({ item }): JSX.Element => renderItem(item)}
          keyExtractor={(item): string => item.id}
          // onRefresh={(): void => load(1)}
          style={styles.list}
          ListHeaderComponent={(): JSX.Element => (
            <Headline>{t('navigation.feed')}</Headline>
          )}
          ListFooterComponent={(): JSX.Element =>
            loading ? <Spinner /> : null
          }
          onScroll={Animated.event([
            { nativeEvent: { contentOffset: { y: scrollY } } }
          ])}
          onEndReached={onEndReached}
        />
      </Content>
    </Container>
  );
}
