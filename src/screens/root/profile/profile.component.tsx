import React from 'react';
import { Text, Container, Content } from 'native-base';

export function Profile(): JSX.Element {
  return (
    <Container>
      <Content>
        <Text>Profile</Text>
      </Content>
    </Container>
  );
}
