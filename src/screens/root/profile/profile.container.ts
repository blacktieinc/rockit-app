import { connect } from 'react-redux';
import { Profile } from './profile.component';

export const ProfileContainer = connect()(Profile);
