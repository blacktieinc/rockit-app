import React, { PropsWithChildren } from 'react';

export function Screens({ children }: PropsWithChildren<{}>): JSX.Element {
  return <>{children}</>;
}
