import React, { useMemo } from 'react';
import moment from 'moment';
import get from 'lodash/get';
import UriJs from 'urijs';
import { useTranslation } from 'react-i18next';
import { Heading, Link } from '@uikit/components';
import { styles } from './heading.styles';

interface FestivalHeadingProps {
  event: DTO.Event;
}

export function FestivalHeading({ event }: FestivalHeadingProps): JSX.Element {
  const { t } = useTranslation();

  const homepage = useMemo(() => {
    return ((href: string): JSX.Element => {
      return href ? <Link href={href}>{new UriJs(href).origin()}</Link> : null;
    })(get(event, 'homepage'));
  }, [event]);

  const tickets = useMemo(() => {
    return ((href: string): JSX.Element => {
      return href ? <Link href={href}>{new UriJs(href).origin()}</Link> : null;
    })(get(event, 'tickets'));
  }, [event]);

  return event ? (
    <Heading
      style={styles.heading}
      title={t('forms.dateRange', {
        start: moment(get(event, 'startDateTime')).format('L'),
        end: moment(get(event, 'endDateTime')).format('L')
      })}
      notes={[
        {
          label: t('forms.homepageColon'),
          value: homepage
        },
        {
          label: t('forms.ticketsColon'),
          value: tickets
        }
      ]}
    />
  ) : null;
}
