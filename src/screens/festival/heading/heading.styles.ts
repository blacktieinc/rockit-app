import { ViewStyle } from 'react-native';
import StyleSheet from 'react-native-extended-stylesheet';

interface Styles {
  heading: ViewStyle;
}

export const styles: Styles = StyleSheet.create({
  heading: {
    marginHorizontal: 16,
    marginBottom: 16
  }
} as Styles) as Styles;
