import gql from 'graphql-tag';

export const LOAD_QUERY = gql`
  query LoadEvent(
    $id: String!
    $logoW: Float!
    $wallpaperW: Float!
    $wallpaperH: Float!
    $artistW: Float!
    $artistH: Float!
  ) {
    getEvent(id: $id) {
      id
      title
      summary
      startDateTime
      endDateTime
      homepage
      tickets
      location {
        address
        geometry {
          lat
          lng
        }
      }
      organizer {
        social {
          facebook
          instagram
          twitter
          telegram
          youtube
        }
      }
      logo(width: $logoW)
      wallpaper(width: $wallpaperW, height: $wallpaperH)
    }
    getEventPerformances(event: $id, page: 1, perPage: 10) {
      results {
        id
        headliner
        artist {
          id
          title
          picture(width: $artistW, height: $artistH)
        }
      }
    }
  }
`;
