import React from 'react';
import { Animated } from 'react-native';
import { Actions } from 'react-native-router-flux';
import { Left, Button, Title, Right } from 'native-base';
import { Header, Icon } from '@uikit/components';

interface FestivalHeaderProps {
  event: DTO.Event;
  scrollY: Animated.Value;
  inputRange: number[];
}

export function FestivalHeader({
  event,
  scrollY,
  inputRange
}: FestivalHeaderProps): JSX.Element {
  return event ? (
    <Header scrollY={scrollY} input={inputRange}>
      <Left>
        <Button transparent onPress={(): void => Actions.pop()}>
          <Icon name='chevron-left' size={16} />
        </Button>
      </Left>
      <Header.Body scrollY={scrollY} input={inputRange}>
        <Title>{event.title}</Title>
      </Header.Body>
      <Right />
    </Header>
  ) : null;
}
