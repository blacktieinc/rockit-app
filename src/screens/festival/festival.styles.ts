import { ViewStyle, Dimensions } from 'react-native';
import StyleSheet from 'react-native-extended-stylesheet';

const { height } = Dimensions.get('screen');

interface Styles {
  content: ViewStyle;
  spinner: ViewStyle;
}

export const styles: Styles = StyleSheet.create({
  content: {
    position: 'absolute',
    zIndex: -1,
    width: '100%',
    height: '100%',
    flex: 1
  },
  spinner: {
    flex: 1,
    height
  }
} as Styles) as Styles;
