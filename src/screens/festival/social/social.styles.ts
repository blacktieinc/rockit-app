import { ViewStyle } from 'react-native';
import StyleSheet from 'react-native-extended-stylesheet';

interface Styles {
  container: ViewStyle;
  button: ViewStyle;
}

export const styles: Styles = StyleSheet.create({
  container: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'center',
    marginBottom: 30,
    marginHorizontal: 16
  },
  button: {
    marginHorizontal: 4
  }
} as Styles) as Styles;
