import get from 'lodash/get';
import React, { useMemo } from 'react';
import { View } from 'native-base';
import { SocialButton } from '@uikit/components';
import { styles } from './social.styles';

interface FestivalSocialProps {
  event: DTO.Event;
}

export function FestivalSocial({ event }: FestivalSocialProps): JSX.Element {
  const socail = useMemo((): JSX.Element => {
    return ((soc: DTO.Social): JSX.Element => {
      return soc ? (
        <>
          {Object.keys(soc)
            .filter((key): boolean => soc[key] && key !== '__typename')
            .map(
              (key): JSX.Element => {
                return (
                  <SocialButton
                    key={key}
                    type={key}
                    href={soc[key]}
                    style={styles.button}
                  />
                );
              }
            )}
        </>
      ) : null;
    })(get(event, 'organizer.social'));
  }, [event]);

  return event ? <View style={styles.container}>{socail}</View> : null;
}
