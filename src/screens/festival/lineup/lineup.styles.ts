import { ViewStyle } from 'react-native';
import StyleSheet from 'react-native-extended-stylesheet';

interface Styles {
  lineup: ViewStyle;
}

export const styles: Styles = StyleSheet.create({
  lineup: {
    marginBottom: 30
  }
} as Styles) as Styles;
