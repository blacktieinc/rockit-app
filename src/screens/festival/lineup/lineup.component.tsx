import React, { useCallback } from 'react';
import { Actions } from 'react-native-router-flux';
import { useTranslation } from 'react-i18next';
import { PreviewList } from '@uikit/components';
import { styles } from './lineup.styles';

interface FestivalLineupProps {
  id: string;
  performances: DTO.Performance[];
}

export function FestivalLineup({
  id,
  performances
}: FestivalLineupProps): JSX.Element {
  const { t } = useTranslation();

  const handelKeyExtractor = useCallback(
    ({ id: pid }: DTO.Performance): string => pid,
    []
  );

  const handelImageExtractor = useCallback(
    ({ artist: { picture } }: DTO.Performance): string => picture,
    []
  );

  const handelTitleExtractor = useCallback(
    ({ artist: { title } }: DTO.Performance): string => title,
    []
  );

  const handelOnItemPress = useCallback(
    ({ artist: { id: aid } }: DTO.Performance): void =>
      Actions.push('artist', { id: aid }),
    []
  );

  const handelOnViewAllPress = useCallback(
    (): void => Actions.push('lineup', { id }),
    [id]
  );

  const handelLabelExtractor = useCallback(
    ({ headliner }: DTO.Performance): string => {
      return headliner ? t('forms.headliner') : '';
    },
    [t]
  );

  return performances && performances.length ? (
    <PreviewList<DTO.Performance>
      style={styles.lineup}
      heading='h3'
      data={performances}
      title={t('navigation.lineup')}
      onViewAllPress={handelOnViewAllPress}
      onItemPress={handelOnItemPress}
      keyExtractor={handelKeyExtractor}
      imageExtractor={handelImageExtractor}
      titleExtractor={handelTitleExtractor}
      labelExtractor={handelLabelExtractor}
    />
  ) : null;
}
