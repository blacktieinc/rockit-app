import get from 'lodash/get';
import React, { useMemo } from 'react';
import { Animated, PixelRatio } from 'react-native';
import { Container } from 'native-base';
import { Spinner } from '@uikit/components';
import { variable } from '@uikit/theme';
import { graphql, ChildDataProps } from '@apollo/react-hoc';
import { FestivalHeader } from './header/header.component';
import { FestivalBanner } from './banner/banner.component';
import { FestivalHeading } from './heading/heading.component';
import { FestivalSocial } from './social/social.component';
import { FestivalSummary } from './summary/summary.component';
import { FestivalLineup } from './lineup/lineup.component';
import { FestivalLocation } from './location/location.component';
import { LOAD_QUERY } from './festival.queries';
import { styles } from './festival.styles';

interface FestivalInput {
  id: string;
}

interface FestivalVariables {
  id: string;
  logoW: number;
  wallpaperW: number;
  wallpaperH: number;
  artistW: number;
  artistH: number;
}

interface FestivalResponse {
  getEvent: DTO.Event;
  getEventPerformances: DTO.Paged<DTO.Performance>;
}

type FestivalProps = ChildDataProps<
  FestivalInput,
  FestivalResponse,
  FestivalVariables
>;

const inputRange = [
  variable.deviceWidth * 0.57 -
    (variable.toolbarHeight + variable.searchBarHeight) * 2,
  variable.deviceWidth * 0.57 -
    (variable.toolbarHeight + variable.searchBarHeight)
];

export function Festival({
  data: {
    getEvent,
    getEventPerformances,
    loading,
    variables: { id }
  }
}: FestivalProps): JSX.Element {
  const scrollY = new Animated.Value(0);

  const performances = useMemo(() => get(getEventPerformances, 'results'), [
    getEventPerformances
  ]);

  const spinner = loading && <Spinner padder center style={styles.spinner} />;

  return (
    <Container>
      <FestivalHeader
        event={getEvent}
        scrollY={scrollY}
        inputRange={inputRange}
      />
      <Animated.ScrollView
        style={styles.content}
        onScroll={Animated.event([
          { nativeEvent: { contentOffset: { y: scrollY } } }
        ])}
      >
        {spinner}
        <FestivalBanner event={getEvent} />
        <FestivalHeading event={getEvent} />
        <FestivalSocial event={getEvent} />
        <FestivalLineup id={id} performances={performances} />
        <FestivalLocation event={getEvent} />
        <FestivalSummary event={getEvent} />
      </Animated.ScrollView>
    </Container>
  );
}

export const FestivalGQL = graphql<
  FestivalProps,
  FestivalResponse,
  FestivalVariables
>(LOAD_QUERY, {
  options: ({ id }) => ({
    variables: {
      id: id || '5da779dac16737231754dcb0',
      logoW: PixelRatio.getPixelSizeForLayoutSize(72),
      wallpaperW: PixelRatio.getPixelSizeForLayoutSize(variable.deviceWidth),
      wallpaperH: PixelRatio.getPixelSizeForLayoutSize(
        variable.deviceWidth * 0.57
      ),
      artistW: PixelRatio.getPixelSizeForLayoutSize(148),
      artistH: PixelRatio.getPixelSizeForLayoutSize(184)
    }
  })
})(Festival);
