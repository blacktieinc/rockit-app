import { ViewStyle, TextStyle } from 'react-native';
import StyleSheet from 'react-native-extended-stylesheet';
import { variable } from '@uikit/theme';

interface Styles {
  container: ViewStyle;
  title: TextStyle;
  note: TextStyle;
  map: ViewStyle;
}

export const styles: Styles = StyleSheet.create({
  container: {
    marginBottom: 30,
    marginHorizontal: 16
  },
  title: {
    marginBottom: 4
  },
  note: {
    fontSize: 11,
    marginBottom: 22
  },
  map: {
    width: variable.deviceWidth - 32,
    height: 150,
    borderRadius: 8
  }
} as Styles) as Styles;
