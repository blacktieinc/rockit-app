import React, { useCallback } from 'react';
import get from 'lodash/get';
import { Actions } from 'react-native-router-flux';
import { TouchableOpacity } from 'react-native';
import MapView, { Marker } from 'react-native-maps';
import { useTranslation } from 'react-i18next';
import { View, H3, Text } from 'native-base';
import { Icon } from '@uikit/components';
import { variable } from '@uikit/theme';
import { styles } from './location.styles';

interface FestivalLocationProps {
  event: DTO.Event;
}

export function FestivalLocation({
  event
}: FestivalLocationProps): JSX.Element {
  const { t } = useTranslation();

  const handleOnPress = useCallback(() => {
    Actions.push('map', { location: get(event, 'location') });
  }, [event]);

  return event ? (
    <View style={styles.container}>
      <H3 style={styles.title}>{t('forms.location')}</H3>
      <Text style={styles.note} note>
        {get(event, 'location.address')}
      </Text>
      <TouchableOpacity onPress={handleOnPress}>
        <MapView
          style={styles.map}
          zoomEnabled={false}
          initialRegion={{
            latitude: get(event, 'location.geometry.lat'),
            longitude: get(event, 'location.geometry.lng'),
            latitudeDelta: 0.03,
            longitudeDelta: 0.03
          }}
        >
          <Marker
            coordinate={{
              latitude: get(event, 'location.geometry.lat'),
              longitude: get(event, 'location.geometry.lng')
            }}
          >
            <Icon name='location' size={40} color={variable.brandPrimary} />
          </Marker>
        </MapView>
      </TouchableOpacity>
    </View>
  ) : null;
}
