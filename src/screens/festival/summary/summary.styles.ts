import { ViewStyle } from 'react-native';
import StyleSheet from 'react-native-extended-stylesheet';

interface Styles {
  summary: ViewStyle;
}

export const styles: Styles = StyleSheet.create({
  summary: {
    marginHorizontal: 16,
    marginBottom: 30
  }
} as Styles) as Styles;
