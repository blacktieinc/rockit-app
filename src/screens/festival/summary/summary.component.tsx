import React from 'react';
import { useTranslation } from 'react-i18next';
import { ExpandableText } from '@uikit/components';
import { styles } from './summary.styles';

interface FestivalSummaryProps {
  event: DTO.Event;
}

export function FestivalSummary({ event }: FestivalSummaryProps): JSX.Element {
  const { t } = useTranslation();

  return event ? (
    <ExpandableText
      transparent
      heading='h3'
      style={styles.summary}
      title={t('forms.summary')}
      text={event.summary}
    />
  ) : null;
}
