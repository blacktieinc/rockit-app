import React from 'react';
import { Banner } from '@uikit/components';

interface FestivalBannerProps {
  event: DTO.Event;
}

export function FestivalBanner({ event }: FestivalBannerProps): JSX.Element {
  return event ? (
    <Banner<DTO.Event>
      item={event}
      titleExtractor={({ title }): string => title}
      wallpaperExtractor={({ wallpaper }): string => wallpaper}
      logoExtractor={({ logo }): string => logo}
    />
  ) : null;
}
