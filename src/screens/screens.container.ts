import { connect } from 'react-redux';
import { Screens } from './screens.component';

export const ScreensContainer = connect()(Screens);
