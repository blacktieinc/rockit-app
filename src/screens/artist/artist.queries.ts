import gql from 'graphql-tag';

export const LOAD_QUERY = gql`
  query LoadArtist($id: String!, $width: Float!, $height: Float!) {
    getArtist(id: $id) {
      id
      title
      content
      homepage
      genres
      country
      picture(width: $width, height: $height)
      socialNetworks {
        type
        url
      }
      youtube {
        type
        url
      }
      purchaseForDownload {
        type
        url
      }
    }
  }
`;
