import React, { useCallback } from 'react';
import { Banner } from '@uikit/components';
import { variable } from '@uikit/theme';

interface ArtistBannerProps {
  artist: DTO.Artist;
}

export function ArtistBanner({ artist }: ArtistBannerProps): JSX.Element {
  const wallpaperExtractor = useCallback(
    ({ picture }: DTO.Artist) => picture,
    []
  );

  const titleExtractor = useCallback(({ title }: DTO.Artist) => title, []);

  return artist ? (
    <Banner<DTO.Artist>
      item={artist}
      wallpaperExtractor={wallpaperExtractor}
      titleExtractor={titleExtractor}
      height={variable.deviceWidth * 1.43}
    />
  ) : null;
}
