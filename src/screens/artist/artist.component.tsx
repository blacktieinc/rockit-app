import React from 'react';
import { graphql, ChildDataProps } from '@apollo/react-hoc';
import { Animated, PixelRatio } from 'react-native';
import { Container } from 'native-base';
import { variable } from '@uikit/theme';
import { Spinner } from '@uikit/components';
import { ArtistBanner } from './banner/banner.component';
import { ArtistsHeading } from './heading/heading.component';
import { ArtistHeader } from './header/header.component';
import { ArtistLinks } from './links/links.component';
import { ArtistSummary } from './summary/summary.component';
import { LOAD_QUERY } from './artist.queries';
import { styles } from './artist.styles';

interface ArtistInput {
  id: string;
}

interface ArtistResponse {
  getArtist: DTO.Artist;
}

interface ArtistVariables {
  id: string;
  width: number;
  height: number;
}

type ArtistProps = ChildDataProps<ArtistInput, ArtistResponse, ArtistVariables>;

const inputRange = [
  variable.deviceWidth * 1.43 -
    (variable.toolbarHeight + variable.searchBarHeight) * 2,
  variable.deviceWidth * 1.43 -
    (variable.toolbarHeight + variable.searchBarHeight)
];

export function Artist({
  data: { getArtist, loading }
}: ArtistProps): JSX.Element {
  const scrollY = new Animated.Value(0);

  return (
    <Container>
      <ArtistHeader
        artist={getArtist}
        scrollY={scrollY}
        inputRange={inputRange}
      />
      <Animated.ScrollView
        style={styles.content}
        onScroll={Animated.event([
          { nativeEvent: { contentOffset: { y: scrollY } } }
        ])}
      >
        <ArtistBanner artist={getArtist} />
        <ArtistsHeading artist={getArtist} />
        <ArtistLinks artist={getArtist} />
        <ArtistSummary artist={getArtist} />
        <Spinner fill center show={loading} />
      </Animated.ScrollView>
    </Container>
  );
}

export const ArtistGQL = graphql<
  ArtistInput,
  ArtistResponse,
  ArtistVariables,
  ArtistProps
>(LOAD_QUERY, {
  options: ({ id }) => ({
    variables: {
      id: id || '5db9ea0c8ebb8e592de95fef',
      width: PixelRatio.getPixelSizeForLayoutSize(variable.deviceWidth),
      height: PixelRatio.getPixelSizeForLayoutSize(variable.deviceWidth * 1.43)
    }
  })
})(Artist);
