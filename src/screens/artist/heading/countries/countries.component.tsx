import React from 'react';
import { PixelRatio } from 'react-native';
import { Flag } from 'react-native-svg-flagkit';
import { View, Text } from 'native-base';
import { useTranslation } from 'react-i18next';
import { styles } from './countries.styles';

interface ArtistCountriesProps {
  countries: string[];
}

export function ArtistCountries({
  countries
}: ArtistCountriesProps): JSX.Element {
  const { t } = useTranslation();

  return (
    <>
      {countries.map(
        (code): JSX.Element => {
          return (
            <View style={styles.container} key={code}>
              <Flag
                width={10}
                height={6}
                size={PixelRatio.get()}
                id={code.toUpperCase()}
              />
              <Text style={styles.text}>
                {t(`countries.${code.toUpperCase()}`)}
              </Text>
            </View>
          );
        }
      )}
    </>
  );
}
