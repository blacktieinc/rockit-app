import { ViewStyle, TextStyle } from 'react-native';
import StyleSheet from 'react-native-extended-stylesheet';

interface Styles {
  container: ViewStyle;
  text: TextStyle;
}

export const styles: Styles = StyleSheet.create({
  container: {
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center'
  },
  text: {
    marginLeft: 6
  }
} as Styles) as Styles;
