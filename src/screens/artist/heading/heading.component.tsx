import React, { useMemo } from 'react';
import UriJs from 'urijs';
import get from 'lodash/get';
import startCase from 'lodash/startCase';
import toLower from 'lodash/toLower';
import { Grid, Row, Col, Text } from 'native-base';
import { useTranslation } from 'react-i18next';
import { Link } from '@uikit/components';
import { ArtistCountries } from './countries/countries.component';
import { styles } from './heading.styles';

interface ArtistsHeadingProps {
  artist: DTO.Artist;
}

export function ArtistsHeading({ artist }: ArtistsHeadingProps): JSX.Element {
  const { t } = useTranslation();

  const homepage = useMemo(() => {
    return ((uri: string): JSX.Element => {
      return uri ? (
        <Row style={styles.row}>
          <Col style={styles.colLabel}>
            <Text note numberOfLines={1}>
              {t('forms.homepageColon')}
            </Text>
          </Col>
          <Col style={styles.colAction}>
            <Link href={uri}>{new UriJs(uri).origin()}</Link>
          </Col>
        </Row>
      ) : null;
    })(get(artist, 'homepage'));
  }, [artist, t]);

  const genres = useMemo(() => {
    return ((list: string[]): JSX.Element => {
      return list.length ? (
        <Row style={styles.row}>
          <Col style={styles.colLabel}>
            <Text note numberOfLines={1}>
              {t('forms.genresColon')}
            </Text>
          </Col>
          <Col style={styles.colAction}>
            <Text>
              {list
                .map((genre): string => startCase(toLower(genre)))
                .join(', ')}
            </Text>
          </Col>
        </Row>
      ) : null;
    })(get(artist, 'genres', []));
  }, [artist, t]);

  const country = useMemo(() => {
    return ((list: string[]): JSX.Element => {
      return list.length ? (
        <Row style={styles.row}>
          <Col style={styles.colLabel}>
            <Text note numberOfLines={1}>
              {t('forms.countryColon')}
            </Text>
          </Col>
          <Col style={styles.colAction}>
            <ArtistCountries countries={list} />
          </Col>
        </Row>
      ) : null;
    })(get(artist, 'country', []));
  }, [artist, t]);

  return artist ? (
    <Grid style={styles.container}>
      {homepage}
      {country}
      {genres}
    </Grid>
  ) : null;
}
