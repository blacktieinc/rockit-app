import React from 'react';
import { useTranslation } from 'react-i18next';
import { ExpandableText } from '@uikit/components';
import { styles } from './summary.styles';

interface ArtistSummaryProps {
  artist: DTO.Artist;
}

export function ArtistSummary({ artist }: ArtistSummaryProps): JSX.Element {
  const { t } = useTranslation();

  return artist ? (
    <ExpandableText
      heading='h3'
      style={styles.summary}
      title={t('forms.summary')}
      text={artist.content}
    />
  ) : null;
}
