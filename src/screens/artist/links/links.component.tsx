import get from 'lodash/get';
import React, { useMemo } from 'react';

import { useTranslation } from 'react-i18next';
import { View, H3 } from 'native-base';
import { SocialButton } from '@uikit/components';
import { styles } from './links.styles';

interface ArtistLinksProps {
  artist: DTO.Artist;
}

export function ArtistLinks({ artist }: ArtistLinksProps): JSX.Element {
  const { t } = useTranslation();

  const links = useMemo(() => {
    const soc = get(artist, 'socialNetworks', []);
    const youtube = get(artist, 'youtube', []).slice(0, 1);
    const download = get(artist, 'purchaseForDownload', []);
    return [...soc, ...youtube, ...download];
  }, [artist]);

  return artist ? (
    <View style={styles.container}>
      <H3 style={styles.title}>{t('forms.links')}</H3>
      <View style={styles.resources}>
        {links.map(
          ({ url, type }): JSX.Element => {
            return (
              <SocialButton
                key={url}
                type={type}
                href={url}
                style={styles.social}
              />
            );
          }
        )}
      </View>
    </View>
  ) : null;
}
