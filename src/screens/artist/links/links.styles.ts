import { ViewStyle, TextStyle } from 'react-native';
import StyleSheet from 'react-native-extended-stylesheet';

interface Styles {
  container: ViewStyle;
  title: TextStyle;
  social: ViewStyle;
  resources: ViewStyle;
}

export const styles: Styles = StyleSheet.create({
  container: {
    paddingHorizontal: 16,
    marginBottom: 26
  },
  title: {
    marginBottom: 14
  },
  resources: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'center',
    flexWrap: 'wrap'
  },
  social: {
    marginHorizontal: 4,
    marginVertical: 4
  }
} as Styles) as Styles;
