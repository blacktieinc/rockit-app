import React from 'react';
import { Formik } from 'formik';
import { useValidationSchema } from './forgot-password.utils';
import { ForgotPassword } from './forgot-password.component';

export function ForgotPasswordContainer(): JSX.Element {
  const { validationSchema } = useValidationSchema();

  return (
    <Formik
      validationSchema={validationSchema}
      initialValues={{ email: '' }}
      onSubmit={(): void => {}}
    >
      {(): JSX.Element => <ForgotPassword />}
    </Formik>
  );
}
