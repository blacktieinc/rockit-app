import React, { useMemo } from 'react';
import { useTranslation } from 'react-i18next';
import { Button, Text, View } from 'native-base';
import { Form } from '@uikit/form';
import { AuthContainer } from '@common/auth';

export function ForgotPassword(): JSX.Element {
  const { t } = useTranslation();

  const note = useMemo(
    () => (
      <Text
        style={{
          fontSize: 14,
          marginBottom: 16,
          lineHeight: 20
        }}
      >
        {t('messages.forgotPassword')}
      </Text>
    ),
    [t]
  );

  const form = useMemo(
    (): JSX.Element => (
      <View style={{ marginBottom: 30 }}>
        <Form>
          <Form.Input
            type='email-address'
            name='email'
            label={t('forms.email')}
          />
        </Form>
      </View>
    ),
    [t]
  );

  const send = useMemo(
    (): JSX.Element => (
      <Button rounded block>
        <Text>{t('buttons.send').toUpperCase()}</Text>
      </Button>
    ),
    [t]
  );

  return (
    <AuthContainer back headline={t('navigation.forgotPassword')}>
      {note}
      {form}
      {send}
    </AuthContainer>
  );
}
