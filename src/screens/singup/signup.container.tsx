import React from 'react';
import { Formik } from 'formik';
import { SignUp } from './signup.component';
import { useValidationSchema } from './signup.utils';

export function SignUpContainer(): JSX.Element {
  const { validationSchema } = useValidationSchema();

  return (
    <Formik
      initialValues={{
        fullName: '',
        email: '',
        password: ''
      }}
      isInitialValid={false}
      validationSchema={validationSchema}
      onSubmit={(): void => {}}
    >
      {({ values, isValid }): JSX.Element => (
        <SignUp values={values} isValid={isValid} />
      )}
    </Formik>
  );
}
