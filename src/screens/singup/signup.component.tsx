import React, { useMemo, useCallback } from 'react';
import { Actions } from 'react-native-router-flux';
import { useTranslation } from 'react-i18next';
import { View, Button, Text } from 'native-base';
import { useMutation } from '@apollo/react-hooks';
import { ApolloError } from 'apollo-client';
import { useErrorExtractor } from '@shared/hooks';
import { Form } from '@uikit/form';
import { Icon, Spinner } from '@uikit/components';
import { useToast } from '@uikit/services';
import { variable } from '@uikit/theme';
import { AuthContainer, styles } from '@common/auth';
import { SIGNUP_MUTATION } from './signup.queries';

interface SignUpProps {
  isValid: boolean;
  values: {
    fullName: string;
    email: string;
    password: string;
  };
}

interface SignUpResponse {
  signUp: DTO.User;
}

interface SignUpVariables {
  firstName: string;
  lastName: string;
  email: string;
  password: string;
}

export function SignUp({ values, isValid }: SignUpProps): JSX.Element {
  const { t } = useTranslation();
  const { extractor } = useErrorExtractor();
  const { danger, success } = useToast();

  const handlePopAction = useCallback(() => Actions.pop(), []);

  const handleOnError = useCallback(
    (error: ApolloError): void => {
      extractor(error).forEach(message => {
        danger({ message, button: t('buttons.ok').toUpperCase() });
      });
    },
    [danger, extractor, t]
  );

  const handleOnComplete = useCallback(
    ({ signUp: { email } }: SignUpResponse) => {
      success({
        message: t('messages.signUpVerification', { email }),
        button: t('buttons.ok').toUpperCase()
      });
      handlePopAction();
    },
    [handlePopAction, success, t]
  );

  const [signUp, { loading }] = useMutation<SignUpResponse, SignUpVariables>(
    SIGNUP_MUTATION,
    {
      onError: handleOnError,
      onCompleted: handleOnComplete
    }
  );

  const handleSignUp = useCallback((): void => {
    (([firstName, lastName]): void => {
      signUp({
        variables: {
          email: values.email.toLowerCase(),
          password: values.password,
          firstName: firstName ? firstName.trim() : '',
          lastName: lastName ? lastName.trim() : ''
        }
      });
    })(values.fullName.split(' '));
  }, [signUp, values]);

  const form = useMemo(
    (): JSX.Element => (
      <Form>
        <Form.Input name='fullName' label={t('forms.fullName')} />
        <Form.Input name='email' label={t('forms.email')} />
        <Form.Input name='password' label={t('forms.password')} secure />
      </Form>
    ),
    [t]
  );

  const actions = useMemo(
    () => (
      <View style={styles.formActions}>
        <View />
        <Button dark transparent small iconRight onPress={handlePopAction}>
          <Text style={styles.formActionText}>
            {t('buttons.alreadyHaveAnAccount')}
          </Text>
          <Icon name='arrow-right' size={8} color={variable.brandPrimary} />
        </Button>
      </View>
    ),
    [handlePopAction, t]
  );

  const signup = useMemo(
    (): JSX.Element => (
      <Button primary rounded block onPress={handleSignUp} disabled={!isValid}>
        <Text>{t('buttons.signUp').toUpperCase()}</Text>
      </Button>
    ),
    [handleSignUp, isValid, t]
  );

  const spinner = useMemo(
    (): JSX.Element => <Spinner show={loading} fill center backdrop />,
    [loading]
  );

  return (
    <>
      {spinner}
      <AuthContainer
        back
        headline={t('navigation.signup')}
        actions={t('forms.orSignUpWithSocialAccount')}
      >
        {form}
        {actions}
        {signup}
      </AuthContainer>
    </>
  );
}
