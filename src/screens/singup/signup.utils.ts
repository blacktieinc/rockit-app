import { useTranslation } from 'react-i18next';
import { object, string, Schema } from 'yup';

export function useValidationSchema(): { validationSchema: Schema<{}> } {
  const { t } = useTranslation();

  const validationSchema = object().shape({
    fullName: string()
      .required(
        t('validation.required', {
          field: t('forms.fullName')
        })
      )
      .test('fullName', t('validation.fullName'), (value): boolean => {
        return /^[a-zA-Z]+ [a-zA-Z]+$/.test(value);
      }),
    email: string()
      .required(
        t('validation.required', {
          field: t('forms.email')
        })
      )
      .email(
        t('validation.email', {
          field: t('forms.email')
        })
      ),
    password: string()
      .required(
        t('validation.required', {
          field: t('forms.password')
        })
      )
      .test(
        'passwordLength',
        t('validation.passwordLength'),
        (value): boolean => {
          return /^(?=.{8,})/.test(value);
        }
      )
      .test(
        'passwordUppercaseCharacter',
        t('validation.passwordUppercaseCharacter'),
        (value): boolean => {
          return /^(?=.*[A-Z])/.test(value);
        }
      )
      .test(
        'passwordNumericCharacter',
        t('validation.passwordNumericCharacter'),
        (value): boolean => {
          return /^(?=.*[0-9])/.test(value);
        }
      )
      .test(
        'passwordSpecialCharacter',
        t('validation.passwordSpecialCharacter'),
        (value): boolean => {
          return /^(?=.[!@#$%^&])/.test(value);
        }
      )
  });

  return { validationSchema };
}
