import get from 'lodash/get';
import merge from 'lodash/merge';
import React, { useMemo, useCallback } from 'react';
import { Actions } from 'react-native-router-flux';
import { Animated, PixelRatio } from 'react-native';
import { Container, Content, Title, View, Left, Button } from 'native-base';
import { useTranslation } from 'react-i18next';
import { graphql, ChildDataProps } from '@apollo/react-hoc';
import {
  Header,
  Headline,
  Spinner,
  ProductCard,
  Icon
} from '@uikit/components';
import { LOAD_QUERY } from './lineup.queries';
import { styles, ArtistPicture } from './lineup.style';

export interface LineupInput {
  id: string;
}

interface LineupResponse {
  getEventPerformances: DTO.Paged<DTO.Performance>;
}

interface LineupVariables {
  id: string;
  width: number;
  height: number;
  page: number;
}

type LineupProps = ChildDataProps<LineupInput, LineupResponse, LineupVariables>;

export function Lineup({
  data: { getEventPerformances, loading, fetchMore }
}: Partial<LineupProps>): JSX.Element {
  const scrollY = new Animated.Value(0);
  const { t } = useTranslation();

  const renderItem = ({
    headliner,
    artist: { id, title, picture }
  }: DTO.Performance): JSX.Element => {
    return (
      <ProductCard
        style={styles.card}
        title={title}
        image={{ uri: picture, ...ArtistPicture }}
        label={headliner ? t('forms.headliner') : null}
        onPress={(): void => Actions.push('artist', { id })}
      />
    );
  };

  const headline = useMemo((): JSX.Element => {
    return (
      <View style={styles.headline}>
        <Headline>{t('navigation.lineup')}</Headline>
      </View>
    );
  }, [t]);

  const footer = useMemo(() => {
    return loading ? <Spinner /> : null;
  }, [loading]);

  const handleOnScroll = Animated.event([
    { nativeEvent: { contentOffset: { y: scrollY } } }
  ]);

  const handleOnEndReached = useCallback(() => {
    if (get(getEventPerformances, 'next') && !loading) {
      fetchMore({
        variables: {
          page: getEventPerformances.next
        },
        updateQuery: (prev, { fetchMoreResult }) => {
          return merge({}, prev, fetchMoreResult, {
            getEventPerformances: {
              results: [
                ...prev.getEventPerformances.results,
                ...fetchMoreResult.getEventPerformances.results
              ]
            }
          });
        }
      });
    }
  }, [fetchMore, getEventPerformances, loading]);

  return (
    <Container>
      <Header scrollY={scrollY}>
        <Left>
          <Button transparent onPress={(): void => Actions.pop()}>
            <Icon name='chevron-left' size={16} />
          </Button>
        </Left>
        <Header.Body scrollY={scrollY}>
          <Title>{t('navigation.lineup')}</Title>
        </Header.Body>
      </Header>
      <Content
        style={styles.content}
        contentContainerStyle={styles.contentContainer}
      >
        <Animated.FlatList
          numColumns={2}
          data={get(getEventPerformances, 'results', [])}
          renderItem={({ item }): JSX.Element => renderItem(item)}
          keyExtractor={(item): string => item.id}
          style={styles.list}
          ListHeaderComponent={headline}
          ListFooterComponent={footer}
          onScroll={handleOnScroll}
          onEndReached={handleOnEndReached}
        />
      </Content>
    </Container>
  );
}

export const LineupGQL = graphql<
  LineupInput,
  LineupResponse,
  LineupVariables,
  LineupProps
>(LOAD_QUERY, {
  options: ({ id }) => ({
    variables: {
      id: id || '5da779dac16737231754dcb0',
      width: PixelRatio.getPixelSizeForLayoutSize(ArtistPicture.width),
      height: PixelRatio.getPixelSizeForLayoutSize(ArtistPicture.height),
      page: 1
    }
  })
})(Lineup);
