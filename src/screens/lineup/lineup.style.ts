import { ViewStyle } from 'react-native';
import StyleSheet from 'react-native-extended-stylesheet';
import { variable } from '@uikit/theme';

interface Styles {
  content: ViewStyle;
  contentContainer: ViewStyle;
  list: ViewStyle;
  headline: ViewStyle;
  card: ViewStyle;
}

export const ArtistPicture = {
  width: (variable.deviceWidth - 48) / 2,
  height: ((variable.deviceWidth - 48) / 2) * 1.3
};

export const styles: Styles = StyleSheet.create({
  content: {
    flex: 1
  },
  contentContainer: {
    flex: 1
  },
  list: {
    paddingHorizontal: 8
  },
  headline: {
    paddingHorizontal: 8
  },
  card: {
    marginHorizontal: 8,
    width: ArtistPicture.width,
    marginBottom: 26
  }
} as Styles) as Styles;
