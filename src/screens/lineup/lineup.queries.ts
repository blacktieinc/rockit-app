import gql from 'graphql-tag';

export const LOAD_QUERY = gql`
  query LoadLineup($id: String!, $page: Int!, $width: Float!, $height: Float!) {
    getEventPerformances(event: $id, page: $page, perPage: 20) {
      next
      results {
        id
        headliner
        artist {
          id
          title
          picture(width: $width, height: $height)
        }
      }
    }
  }
`;
