import { connect } from 'react-redux';
import { UserActions } from '@store/user';
import { Splash, SplashProps } from './splash.component';

const mapDispatchToProps = (dispatch): Partial<SplashProps> => ({
  logIn: (): void => dispatch(new UserActions.LogIn())
});

export const SplashContainer = connect(null, mapDispatchToProps)(Splash);
