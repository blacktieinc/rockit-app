import React, { useEffect } from 'react';

export interface SplashProps {
  logIn: () => void;
}

export function Splash({ logIn }: SplashProps): JSX.Element {
  useEffect((): void => {
    logIn();
  }, [logIn]);

  return <></>;
}
