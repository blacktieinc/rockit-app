/* eslint-disable @typescript-eslint/no-var-requires */
import React, { PropsWithChildren, useMemo, useCallback } from 'react';
import { Image } from 'react-native';
import {
  View,
  Text,
  Button,
  Container,
  Header,
  Content,
  Left,
  Body
} from 'native-base';
import { Actions } from 'react-native-router-flux';
import { Headline, Icon } from '@uikit/components';
import { styles } from './auth.styles';

const google = require('../../assets/google.png');
const facebook = require('../../assets/facebook.png');

export interface AuthProps {
  headline: string;
  back?: boolean;
  actions?: string;
  onFacebookLogin?: () => void;
  onGoogleLogin?: () => void;
}

export function Auth({
  back,
  headline,
  actions,
  children,
  onFacebookLogin,
  onGoogleLogin
}: PropsWithChildren<AuthProps>): JSX.Element {
  const handlePop = useCallback(() => Actions.pop(), []);

  const pop = useMemo(() => {
    return back ? (
      <Left>
        <Button transparent onPress={handlePop}>
          <Icon name='chevron-left' />
        </Button>
      </Left>
    ) : null;
  }, [back, handlePop]);

  const bottom = useMemo(() => {
    return actions ? (
      <View style={styles.bottom}>
        <Text style={styles.label}>{actions}</Text>
        <View style={styles.actions}>
          <Button light style={styles.button} onPress={onGoogleLogin}>
            <Image source={google} style={styles.icon} />
          </Button>
          <Button light style={styles.button} onPress={onFacebookLogin}>
            <Image source={facebook} style={styles.icon} />
          </Button>
        </View>
      </View>
    ) : null;
  }, [actions, onFacebookLogin, onGoogleLogin]);

  return (
    <Container>
      <Header transparent>
        {pop}
        <Body />
      </Header>
      <Content
        style={styles.content}
        contentContainerStyle={styles.contentContainer}
      >
        <Headline style={styles.headline}>{headline}</Headline>
        <View style={styles.container}>
          <View style={styles.children}>{children}</View>
        </View>
        {bottom}
      </Content>
    </Container>
  );
}
