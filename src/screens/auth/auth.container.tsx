/* eslint-disable react/jsx-props-no-spreading */
import React, { PropsWithChildren, useCallback } from 'react';
import { Auth, AuthProps } from './auth.component';
import {
  getFacebookUser,
  loginWithFacebook,
  loginWithGoogle
} from './auth.utils';

type AuthContainerProps = Omit<AuthProps, 'onFacebookLogin' | 'onGoogleLogin'>;

export function AuthContainer({
  children,
  ...rest
}: PropsWithChildren<AuthContainerProps>): JSX.Element {
  const handleFacebooklogin = useCallback((): void => {
    loginWithFacebook()
      .then(
        ({ isCancelled }): Promise<FB.Me> => {
          if (!isCancelled) {
            return getFacebookUser();
          }
          return Promise.reject();
        }
      )
      .then((result): void => {
        console.log(result);
      });
  }, []);

  const handleGoogleLogin = useCallback(() => {
    loginWithGoogle()
      .then((data): void => {
        console.log(data);
      })
      .catch(err => {
        console.log(err);
      });
  }, []);

  return (
    <Auth
      {...rest}
      onFacebookLogin={handleFacebooklogin}
      onGoogleLogin={handleGoogleLogin}
    >
      {children}
    </Auth>
  );
}
