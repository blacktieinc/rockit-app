import { useCallback } from 'react';
import { ApolloError } from 'apollo-client';
import { useTranslation } from 'react-i18next';
import { useMutation } from '@apollo/react-hooks';
import { useToast } from '@uikit/services';
import { useErrorExtractor } from '../hooks/error-extractor.hook';
import { setSession } from './auth.utils';
import { SIGN_IN_MUTATION } from './sign.queries';

interface SignInResponse {
  signIn: DTO.Token;
}

type SignIn = [(values: DTO.Credentials) => void, boolean];

export function useSignIn(callback: () => void): SignIn {
  const { danger } = useToast();
  const { t } = useTranslation();
  const { extractor } = useErrorExtractor();

  const handleOnCompleted = useCallback(
    ({ signIn: { token } }: SignInResponse) => {
      setSession(token).then(() => {
        callback();
      });
    },
    [callback]
  );

  const handleOnError = useCallback(
    (error: ApolloError): void => {
      extractor(error).forEach(message => {
        danger({ message, button: t('buttons.ok').toUpperCase() });
      });
    },
    [danger, extractor, t]
  );

  const [mutate, { loading }] = useMutation<SignInResponse, DTO.Credentials>(
    SIGN_IN_MUTATION,
    {
      onError: handleOnError,
      onCompleted: handleOnCompleted
    }
  );

  const signIn = useCallback(
    (variables: DTO.Credentials) => {
      mutate({ variables });
    },
    [mutate]
  );

  return [signIn, loading];
}
