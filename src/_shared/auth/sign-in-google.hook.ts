import env from 'react-native-config';
import { useCallback } from 'react';
import { GoogleSignin } from '@react-native-community/google-signin';
import { SocialSignIn } from './auth.utils';

GoogleSignin.configure({
  webClientId: env.GOOGLE_OAUTH_CLIEN_ID
});

export function useSignInGoogle(callback: () => void): SocialSignIn {
  const handleSignIn = useCallback(() => {
    GoogleSignin.signIn().then(callback);
  }, [callback]);
  return [handleSignIn];
}
