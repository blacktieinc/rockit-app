import { useCallback } from 'react';
import { LoginManager, LoginResult } from 'react-native-fbsdk';
import { SocialSignIn } from './auth.utils';

export function useSignInFacebook(callback: () => void): SocialSignIn {
  const handleSignIn = useCallback(() => {
    LoginManager.logInWithPermissions(['public_profile', 'email']).then(
      ({ isCancelled }: LoginResult) => {
        if (!isCancelled) callback();
      }
    );
  }, [callback]);
  return [handleSignIn];
}
