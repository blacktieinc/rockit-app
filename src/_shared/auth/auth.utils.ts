import AsyncStorage from '@react-native-community/async-storage';

const SESSION_KEY = 'rockit@token';

export type SocialSignIn = [() => void];

export interface Session {
  token: string;
  type: string;
}

export function setSession(token: string): Promise<void> {
  return AsyncStorage.setItem(SESSION_KEY, token);
}

export function getSession(): Promise<string> {
  return AsyncStorage.getItem(SESSION_KEY);
}
