import env from 'react-native-config';
import { Platform } from 'react-native';
import { ApolloClient } from 'apollo-client';
import { InMemoryCache } from 'apollo-cache-inmemory';
import { HttpLink } from 'apollo-link-http';
import { setContext } from 'apollo-link-context';
import { getSession } from '@shared/auth';

const cache = new InMemoryCache();

const httpLink = new HttpLink({
  // uri: Platform.OS === 'ios' ? env.GRAPH_URL_IOS : env.GRAPH_URL_ANDROID
  uri: 'http://172.18.20.70:3000/graph/v1'
});

const authLink = setContext(() => {
  return getSession().then(token => ({
    headers: {
      authorization: `Bearer ${token}`
    }
  }));
});

export const AppClient = new ApolloClient({
  cache,
  link: authLink.concat(httpLink),
  name: 'rockit-app-client'
});
