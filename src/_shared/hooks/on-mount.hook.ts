/* eslint-disable react-hooks/exhaustive-deps */
import { useEffect } from 'react';

export function useOnMount(callback: () => void): void {
  useEffect((): void => {
    callback();
  }, []);
}
