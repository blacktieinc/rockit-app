import get from 'lodash/get';
import camelCase from 'lodash/camelCase';
import { useCallback } from 'react';
import { useTranslation } from 'react-i18next';
import { ApolloError } from 'apollo-client';
import { GraphQLError } from 'graphql';

interface Extractor {
  extractor: (error: ApolloError, params?: any) => string[];
}

export function useErrorExtractor(): Extractor {
  const { t } = useTranslation();

  const extractor = useCallback(
    (error: ApolloError, params: any = {}): string[] => {
      return get(error, 'networkError.result.errors', []).map(
        ({ message }: GraphQLError) => t(`errors.${camelCase(message)}`, params)
      );
    },
    [t]
  );

  return { extractor };
}
