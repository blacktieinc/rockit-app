import { combineReducers } from 'redux';
import { RootState, rootReducer } from './root/root.reducer';
import { UserState, userReducer } from './user/user.reducer';

export interface AppState {
  root: RootState;
  user: UserState;
}

export const appReducer = combineReducers<AppState>({
  root: rootReducer,
  user: userReducer
});
