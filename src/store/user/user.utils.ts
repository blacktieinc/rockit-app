import gql from 'graphql-tag';
import { AppClient } from '@shared/client';
import { GraphRequest, GraphRequestManager } from 'react-native-fbsdk';
import { GoogleSignin } from '@react-native-community/google-signin';

interface LoginWithFacebookResponse {
  signInWithFacebook?: DTO.Token;
  signInWithGoogle?: DTO.Token;
}

export function signInWithSocialNetwork(
  variables: DTO.SocialSignIn,
  network: string
): Promise<DTO.Token> {
  return AppClient.mutate<LoginWithFacebookResponse, DTO.SocialSignIn>({
    variables,
    mutation: gql`
      mutation SignInWithSocialNetwork(
        $id: String!
        $email: String!
        $firstName: String!
        $lastName: String!
      ) {
        signInWith${network}(
          user: {
            id: $id
            email: $email
            firstName: $firstName
            lastName: $lastName
          }
        ) {
          token
        }
      }
    `
  }).then(
    ({ data: { signInWithFacebook, signInWithGoogle } }) =>
      signInWithFacebook || signInWithGoogle
  );
}

export function getMe(): Promise<DTO.User> {
  return AppClient.query<{ me: DTO.User }>({
    query: gql`
      query Me {
        me {
          id
          email
          facebookId
          googleId
          auth
          name {
            first
          }
        }
      }
    `
  }).then(({ data: { me } }) => me);
}

export function getGoogleMe(): Promise<DTO.SocialSignIn> {
  return GoogleSignin.getCurrentUser().then(
    ({ user }): DTO.SocialSignIn => ({
      id: user.id,
      email: user.email,
      firstName: user.givenName,
      lastName: user.familyName
    })
  );
}

export function getFacebookMe(): Promise<DTO.SocialSignIn> {
  return new Promise<FB.Me>((resolve, reject) => {
    const infoRequest = new GraphRequest(
      '/me',
      {
        parameters: {
          fields: {
            string: 'id,first_name,last_name,email,picture.type(large)'
          }
        }
      },
      (error, result): void => {
        if (error) reject(error);
        else resolve(result as FB.Me);
      }
    );
    new GraphRequestManager().addRequest(infoRequest).start();
  }).then(
    (user: FB.Me): DTO.SocialSignIn => ({
      id: user.id,
      email: user.email,
      firstName: user.first_name,
      lastName: user.last_name
    })
  );
}
