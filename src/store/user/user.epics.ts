import SplashScreen from 'react-native-splash-screen';
import { Observable, EMPTY } from 'rxjs';
import { Actions } from 'react-native-router-flux';
import { Action } from 'redux';
import { ofType, combineEpics, ActionsObservable } from 'redux-observable';
import { flatMap, mergeMap, tap, map } from 'rxjs/operators';
import { setSession } from '@shared/auth';
import {
  getMe,
  getFacebookMe,
  getGoogleMe,
  signInWithSocialNetwork
} from './user.utils';
import * as UserActions from './user.actions';

const loggedInEpic = (
  action$: ActionsObservable<Action>
): Observable<never> => {
  return action$.pipe(
    ofType(UserActions.Types.LOGGED_IN),
    tap((): void => {
      Actions.replace('root');
      SplashScreen.hide();
    }),
    flatMap((): Observable<never> => EMPTY)
  );
};

const logInWithSocialNetworkEpic = (
  action$: ActionsObservable<UserActions.LogInWithSocialNetwork>
): Observable<Action> => {
  return action$.pipe(
    ofType(UserActions.Types.LOG_IN_WITH_SOCIAL_NETWORK),
    mergeMap(
      ({ network }): Promise<[string, DTO.SocialSignIn]> => {
        return ((): Promise<DTO.SocialSignIn> => {
          if (network === 'Facebook') return getFacebookMe();
          return getGoogleMe();
        })().then((user): [string, DTO.SocialSignIn] => [network, user]);
      }
    ),
    flatMap(
      ([network, user]): Promise<DTO.Token> =>
        signInWithSocialNetwork(user, network)
    ),
    flatMap(({ token }): Promise<void> => setSession(token)),
    map((): UserActions.LogIn => new UserActions.LogIn())
  );
};

const logInEpic = (action$: ActionsObservable<Action>): Observable<Action> => {
  return action$.pipe(
    ofType(UserActions.Types.LOG_IN),
    flatMap((): Promise<DTO.User> => getMe()),
    mergeMap((user): Action[] => {
      return [new UserActions.SetUser(user), new UserActions.LoggedIn()];
    })
  );
};

export const userEpics = combineEpics(
  logInEpic,
  loggedInEpic,
  logInWithSocialNetworkEpic
);
