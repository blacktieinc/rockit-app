import { Action } from 'redux';

export enum Types {
  LOG_IN = '[USER] LOG_IN',
  LOG_IN_WITH_SOCIAL_NETWORK = '[USER] LOG_IN_WITH_SOCIAL_NETWORK',
  LOGGED_IN = '[USER] LOGGED_IN',
  SET_USER = '[USER] SET_USER'
}

export class LogIn implements Action {
  public readonly type = Types.LOG_IN;
}

export class LogInWithSocialNetwork implements Action {
  public readonly type = Types.LOG_IN_WITH_SOCIAL_NETWORK;
  public constructor(public network: string) {}
}

export class SetUser implements Action {
  public readonly type = Types.SET_USER;
  public constructor(public user: DTO.User) {}
}

export class LoggedIn implements Action {
  public readonly type = Types.LOGGED_IN;
}

export type All = Types & LogIn & SetUser & LoggedIn & LogInWithSocialNetwork;
