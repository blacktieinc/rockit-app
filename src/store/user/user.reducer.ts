import { All, Types } from './user.actions';

export interface UserState {
  user: DTO.User;
  loading: boolean;
}

export const userInitialState: UserState = {
  loading: false,
  user: null
};

export function userReducer(
  state: UserState = userInitialState,
  action: All
): UserState {
  switch (action.type) {
    case Types.LOG_IN:
    case Types.LOG_IN_WITH_SOCIAL_NETWORK:
      return { ...state, loading: true };
    case Types.SET_USER:
      return { ...state, user: action.user };
    default:
      return state;
  }
}
