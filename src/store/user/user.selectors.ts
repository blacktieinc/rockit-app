import { UserState } from './user.reducer';
import { appSelectors } from '../app.selectors';

const select = appSelectors.fromUserState;

const Selectors = {
  loading: ({ loading }: UserState): boolean => loading,
  user: ({ user }: UserState): DTO.User => user
};

export const UserSelectors = {
  loading: select(Selectors.loading),
  user: select(Selectors.user)
};
