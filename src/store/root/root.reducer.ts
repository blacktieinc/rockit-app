export interface RootState {
  test: string;
}

export const rootInitialState: RootState = {
  test: 'test'
};

export function rootReducer(
  state: RootState = rootInitialState,
  action: any
): RootState {
  switch (action.type) {
    default:
      return state;
  }
}
