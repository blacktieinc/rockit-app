import { Action } from 'redux';

export enum Types {
  HTTP_ERROR = '[ROOT] HTTP_ERROR'
}

export class HttpError implements Action {
  public readonly type = Types.HTTP_ERROR;
  public constructor(public errors: string[]) {}
}

export type All = Types & HttpError;
