import { createSelector, Selector, OutputSelector } from 'reselect';
import { AppState } from './app.state';
import { UserState } from './user/user.reducer';

export type StateSelector<T, S> = OutputSelector<AppState, T, (res: S) => T>;

export const appSelectors = {
  fromUserState: <T>(
    selector: Selector<UserState, T>
  ): StateSelector<T, UserState> => {
    return createSelector(
      ({ user }: AppState): UserState => user,
      selector
    );
  }
};
