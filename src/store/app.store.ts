import { createStore, applyMiddleware, AnyAction, Dispatch } from 'redux';
import { createEpicMiddleware } from 'redux-observable';
import { composeWithDevTools } from 'redux-devtools-extension';

import { appReducer } from './app.state';

import { epic } from './app.epic';

type AnyActionMap = (action: AnyAction) => AnyAction;
type AnyActionDispatcher = (next: Dispatch<AnyAction>) => AnyActionMap;

const epicMiddleware = createEpicMiddleware();

const middlewares = composeWithDevTools(
  applyMiddleware((): AnyActionDispatcher => {
    return (next: Dispatch<AnyAction>): AnyActionMap => {
      return (action: AnyAction): AnyAction => next({ ...action });
    };
  }, epicMiddleware)
);

const store = createStore(appReducer, middlewares);

epicMiddleware.run(epic);

export { store };
