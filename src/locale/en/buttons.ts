export const buttons = {
  viewAll: 'View All',
  viewMore: 'View More',
  viewLess: 'View Less',
  viewOnMap: 'View On Map',
  login: 'Login',
  signUp: 'Sign up',
  forgotYourPassword: 'Forgot your password?',
  alreadyHaveAnAccount: 'Already have an account?',
  send: 'Send',
  ok: 'Ok'
};
