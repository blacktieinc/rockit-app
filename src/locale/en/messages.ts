export const messages = {
  signUpVerification: `A verification email has been sent to {{email}}`,
  forgotPassword:
    'Please, enter your email address. You will receive a link to create a new password via email.'
};
