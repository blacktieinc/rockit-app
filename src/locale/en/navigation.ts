export const navigation = {
  festivals: 'Festivals',
  lineup: 'Lineup',
  feed: 'Feed',
  login: 'Login',
  signup: 'Sign up',
  forgotPassword: 'Forgot password'
};
