export const errors = {
  incorrectEmail: 'User with email {{email}} has not been found',
  theEmailAddressHasNotBeenVerified:
    'The email address {{email}} has not been verified',
  theEmailAddressYouHaveEnteredIsAlreadyAssociatedWithAnotherAccount:
    'The email address you have entered is already associated with another account.'
};
