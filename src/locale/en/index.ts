import { card } from './card';
import { forms } from './forms';
import { navigation } from './navigation';
import { buttons } from './buttons';
import { countries } from './countries';
import { validation } from './validation';
import { messages } from './messages';
import { errors } from './errors';

export const en = {
  card,
  forms,
  navigation,
  buttons,
  countries,
  validation,
  messages,
  errors
};
