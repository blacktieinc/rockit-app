export const validation = {
  required: 'Field {{field}} is required to be entered',
  email: 'Field {{field}} must be a valid email address',
  fullName: 'Please enter your full name (First and Last Name)',
  passwordLength: 'The string must be eight characters or longer',
  passwordUppercaseCharacter:
    'The string must contain at least 1 uppercase character',
  passwordNumericCharacter:
    'The string must contain at least 1 numeric character',
  passwordSpecialCharacter:
    'The string must contain at least one special character'
};
