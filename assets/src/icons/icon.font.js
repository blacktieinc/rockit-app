module.exports = {
  files: ['./*.svg'],
  fontName: 'RockIt',
  classPrefix: 'rct-',
  baseSelector: '.rct',
  types: ['ttf'],
  fileName: 'fonts/[fontname].[ext]'
};
